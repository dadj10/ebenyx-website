package com.ebenyx.config;

import java.io.UnsupportedEncodingException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.ebenyx.entities.Parametre;
import com.ebenyx.repositories.ParametreRepository;

public class EmailUtility {

	// Logger
	public static final Logger log = LoggerFactory.getLogger(EmailUtility.class);

	@Autowired
	public ParametreRepository parametreRepository;

	public static String password = null;

	public boolean main(String destinataire, String subject, String messageText, ParametreRepository parametreRepos)
			throws AddressException, MessagingException {

		Parametre smtp = null;

		// Je recupère les parametres smtp configurés
		smtp = parametreRepos.get();

		// Je declare mes paramètres mail
		String port = null;
		String expediteur = null;
		String passMail = null;
		String applicationName = null;
		String serverHost = null;

		if (smtp != null) {
			log.info("Message non-envoyé ...");
			// Config params
			port = smtp.getMailPort();
			expediteur = smtp.getMailUsername();
			passMail = smtp.getMailPassword();
			applicationName = smtp.getApplicationName();
			serverHost = smtp.getMailHost();
		} else {
			// Default params en contantes
			port = Constantes.DEFAULT_SMTP_PORT;
			expediteur = Constantes.DEFAULT_SMTP_EMAIL;
			passMail = Constantes.DEFAULT_SMTP_PASSWORD;
			applicationName = Constantes.DEFAULT_SERVER_NAME;
			serverHost = Constantes.DEFAULT_SMTP_HOST;
		}

		password = passMail;

		// L'identifiant de messagerie du destinataire doit être mentionné.
		String to = destinataire;

		// L'identifiant de messagerie de l'expéditeur doit être mentionné
		String from = expediteur;

		// En supposant que vous envoyez des e-mails à partir de gmails smtp
		// String host = "smtp.gmail.com";
		String host = serverHost;

		// Obtenir les propriétés du système
		Properties properties = System.getProperties();

		// Configurer le serveur de messagerie
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.ssl.enable", Boolean.TRUE);
		properties.put("mail.smtp.auth", Boolean.TRUE);
		properties.put("mail.debug", Boolean.TRUE);
		properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

		// Récupère l'objet Session.// et passe le nom d'utilisateur et le mot de passe
		Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, password);
			}
		});

		System.out.println("--> host : " + host);
		System.out.println("--> port : " + port);
		System.out.println("--> expediteur : " + from);
		System.out.println("--> destinataire : " + to);
		System.out.println("--> passMail : " + password);

		// Utilisé pour déboguer les problèmes SMTP
		session.setDebug(Boolean.TRUE);

		try {
			// Créez un objet MimeMessage par défaut.
			MimeMessage message = new MimeMessage(session);

			// Définir From: champ d'en-tête de l'en-tête.
			message.setFrom(new InternetAddress(from, applicationName));

			// Définir to: champ d'en-tête de l'en-tête.
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

			// Définir l'objet: champ d'en-tête
			message.setSubject(subject);

			// Maintenant, définissez le message réel
			message.setContent(messageText, "text/html; charset=utf-8");

			System.out.println("Envoi en cours...");

			// Envoyer le message
			Transport.send(message);
			System.out.println("Message envoyé avec succès ...");
			log.info("Message envoyé avec succès ...");
			return true;
		} catch (MessagingException mex) {
			log.info("Message non-envoyé ...");
			log.error(mex.getMessage());
			mex.printStackTrace();
		} catch (UnsupportedEncodingException uee) {
			log.info("Message non-envoyé ...");
			log.error(uee.getMessage());
			uee.printStackTrace();
		} catch (Exception e) {
			log.error(e.getMessage());
		}

		return false;

	}
}
