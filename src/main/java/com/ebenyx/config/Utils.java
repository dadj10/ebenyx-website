package com.ebenyx.config;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utils {

	// Logger
	private static final Logger log = LoggerFactory.getLogger(Utils.class);

	/**
	 * Augmenter une date jour par jour.
	 */
	public static Date addDays(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);
		return cal.getTime();
	}

	/**
	 * Augmenter une date jour par mintes.
	 */
	public static Date addMinutes(Date date, int minute) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MINUTE, minute);
		return cal.getTime();
	}

	/**
	 * Augmenter une date jour par mois.
	 */
	public Date addMonth(Date date, int month) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MINUTE, month);
		return cal.getTime();
	}

	public static boolean isNumeric(String str) {
		try {
			Double.parseDouble(str);
			return true;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			return false;
		}
	}

	/**
	 * Cette fonction permet de retirer tous les caratères spéciaux pour les
	 * remplacer par "_" .
	 */
	public static String removeSpecialChars(String str) {
		String specialChars = "[- + ^ # @ ? ¿ , ; . : / \\ = + ± ù % ‰ $ * ¥ `£  ) ° -] *";
		return str.replaceAll(specialChars, "_");
	}

	/**
	 * Cette fonction permet de tester la connexion a internet.
	 */
	public static boolean netIsAvailable() {
		try {
			final URL url = new URL("http://www.google.com");
			final URLConnection conn = url.openConnection();
			conn.connect();
			conn.getInputStream().close();
			return true;
		} catch (MalformedURLException mue) {
			log.error(mue.getMessage());
			throw new RuntimeException(mue);
		} catch (IOException ioe) {
			log.error(ioe.getMessage());
			return false;
		}
	}

	// Cette fonction permet de verifier qu'une chaine de caractere est un adresse
	// email.
	public static boolean isValidEmailAddress(String str) {
		String mPattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		Pattern pattern = Pattern.compile(mPattern);
		Matcher matcher = pattern.matcher(str);
		return matcher.matches();
	}

	public static String getUsernameFromEmail(String email) {
		try {
			return email.split("@")[0];
		} catch (Exception e) {
			log.error(e.getMessage());
			return email;
		}
	}

	public static String getResume(String contenuActualite, int longeurResumerArticle) {
		String resume = contenuActualite.trim();
		try {
			resume = resume.substring(0, longeurResumerArticle);
			System.out.println("Le résumé est : " + resume);
		} catch (Exception e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		return resume + "...";
	}

	public static class Translate {
		public String item, itemTrans;

		public Translate() {
			super();
		}

		public Translate(String item, String itemTrans) {
			this.item = item;
			this.itemTrans = itemTrans;
		}

		public String getItem() {
			return item;
		}

		public void setItem(String item) {
			this.item = item;
		}

		public String getItemTrans() {
			return itemTrans;
		}

		public void setItemTrans(String itemTrans) {
			this.itemTrans = itemTrans;
		}

		public List<Translate> iteration() {
			List<Translate> list = new ArrayList<Translate>();
			list.add(new Translate("Œ", "O"));
				list.add(new Translate(" ", "-"));
				list.add(new Translate("Ĳ", "I"));
				list.add(new Translate("Ö", "O"));
				list.add(new Translate("Œ", "O"));
				list.add(new Translate("Ü", "U"));
				list.add(new Translate("ä", "a"));
				list.add(new Translate("æ", "a"));
				list.add(new Translate("ĳ", "i"));
				list.add(new Translate("ö", "o"));
				list.add(new Translate("œ", "o"));
				list.add(new Translate("ü", "u"));
				list.add(new Translate("ß", "s"));
				list.add(new Translate("ſ", "s"));
				list.add(new Translate("À", "A"));
				list.add(new Translate("Á", "A"));
				list.add(new Translate("Â", "A"));
				list.add(new Translate("Ã", "A"));
				list.add(new Translate("Ä", "A"));
				list.add(new Translate("Å", "A"));
				list.add(new Translate("Æ", "A"));
				list.add(new Translate("Ā", "A"));
				list.add(new Translate("Ą", "A"));
				list.add(new Translate("Ă", "A"));
				list.add(new Translate("Ç", "C"));
				list.add(new Translate("Ć", "C"));
				list.add(new Translate("Č", "C"));
				list.add(new Translate("Ĉ", "C"));
				list.add(new Translate("Ċ", "C"));
				list.add(new Translate("Ď", "D"));
				list.add(new Translate("Đ", "D"));
				list.add(new Translate("È", "E"));
				list.add(new Translate("É", "E"));
				list.add(new Translate("Ê", "E"));
				list.add(new Translate("Ë", "E"));
				list.add(new Translate("Ē", "E"));
				list.add(new Translate("Ę", "E"));
				list.add(new Translate("Ě", "E"));
				list.add(new Translate("Ĕ", "E"));
				list.add(new Translate("Ė", "E"));
				list.add(new Translate("Ĝ", "G"));
				list.add(new Translate("Ğ", "G"));
				list.add(new Translate("Ġ", "G"));
				list.add(new Translate("Ģ", "G"));
				list.add(new Translate("Ĥ", "H"));
				list.add(new Translate("Ħ", "H"));
				list.add(new Translate("Ì", "I"));
				list.add(new Translate("Í", "I"));
				list.add(new Translate("Î", "I"));
				list.add(new Translate("Ï", "I"));
				list.add(new Translate("Ī", "I"));
				list.add(new Translate("Ĩ", "I"));
				list.add(new Translate("Ĭ", "I"));
				list.add(new Translate("Į", "I"));
				list.add(new Translate("İ", "I"));
				list.add(new Translate("Ĵ", "J"));
				list.add(new Translate("Ķ", "K"));
				list.add(new Translate("Ľ", "K"));
				list.add(new Translate("Ĺ", "K"));
				list.add(new Translate("Ļ", "K"));
				list.add(new Translate("Ŀ", "K"));
				list.add(new Translate("Ł", "L"));
				list.add(new Translate("Ñ", "N"));
				list.add(new Translate("Ń", "N"));
				list.add(new Translate("Ň", "N"));
				list.add(new Translate("Ņ", "N"));
				list.add(new Translate("Ŋ", "N"));
				list.add(new Translate("Ò", "O"));
				list.add(new Translate("Ó", "O"));
				list.add(new Translate("Ô", "O"));
				list.add(new Translate("Õ", "O"));
				list.add(new Translate("Ø", "O"));
				list.add(new Translate("Ō", "O"));
				list.add(new Translate("Ő", "O"));
				list.add(new Translate("Ŏ", "O"));
				list.add(new Translate("Ŕ", "R"));
				list.add(new Translate("Ř", "R"));
				list.add(new Translate("Ŗ", "R"));
				list.add(new Translate("Ś", "S"));
				list.add(new Translate("Ş", "S"));
				list.add(new Translate("Ŝ", "S"));
				list.add(new Translate("Ș", "S"));
				list.add(new Translate("Š", "S"));
				list.add(new Translate("Ť", "T"));
				list.add(new Translate("Ţ", "T"));
				list.add(new Translate("Ŧ", "T"));
				list.add(new Translate("Ț", "T"));
				list.add(new Translate("Ù", "U"));
				list.add(new Translate("Ú", "U"));
				list.add(new Translate("Û", "U"));
				list.add(new Translate("Ū", "U"));
				list.add(new Translate("Ů", "U"));
				list.add(new Translate("Ű", "U"));
				list.add(new Translate("Ŭ", "U"));
				list.add(new Translate("Ũ", "U"));
				list.add(new Translate("Ų", "U"));
				list.add(new Translate("Ŵ", "W"));
				list.add(new Translate("Ŷ", "Y"));
				list.add(new Translate("Ÿ", "Y"));
				list.add(new Translate("Ý", "Y"));
				list.add(new Translate("Ź", "Z"));
				list.add(new Translate("Ż", "Z"));
				list.add(new Translate("Ž", "Z"));
				list.add(new Translate("à", "a"));
				list.add(new Translate("á", "a"));
				list.add(new Translate("â", "a"));
				list.add(new Translate("ã", "a"));
				list.add(new Translate("ā", "a"));
				list.add(new Translate("ą", "a"));
				list.add(new Translate("ă", "a"));
				list.add(new Translate("å", "a"));
				list.add(new Translate("ç", "c"));
				list.add(new Translate("ć", "c"));
				list.add(new Translate("č", "c"));
				list.add(new Translate("ĉ", "c"));
				list.add(new Translate("ċ", "c"));
				list.add(new Translate("ď", "d"));
				list.add(new Translate("đ", "d"));
				list.add(new Translate("è", "e"));
				list.add(new Translate("é", "e"));
				list.add(new Translate("ê", "e"));
				list.add(new Translate("ë", "e"));
				list.add(new Translate("ē", "e"));
				list.add(new Translate("ę", "e"));
				list.add(new Translate("ě", "e"));
				list.add(new Translate("ĕ", "e"));
				list.add(new Translate("ė", "e"));
				list.add(new Translate("ƒ", "f"));
				list.add(new Translate("ĝ", "g"));
				list.add(new Translate("ğ", "g"));
				list.add(new Translate("ġ", "g"));
				list.add(new Translate("ģ", "g"));
				list.add(new Translate("ĥ", "h"));
				list.add(new Translate("ħ", "h"));
				list.add(new Translate("ì", "i"));
				list.add(new Translate("í", "i"));
				list.add(new Translate("î", "i"));
				list.add(new Translate("ï", "i"));
				list.add(new Translate("ī", "i"));
				list.add(new Translate("ĩ", "i"));
				list.add(new Translate("ĭ", "i"));
				list.add(new Translate("į", "i"));
				list.add(new Translate("ı", "i"));
				list.add(new Translate("ĵ", "j"));
				list.add(new Translate("ķ", "k"));
				list.add(new Translate("ĸ", "k"));
				list.add(new Translate("ł", "l"));
				list.add(new Translate("ľ", "l"));
				list.add(new Translate("ĺ", "l"));
				list.add(new Translate("ļ", "l"));
				list.add(new Translate("ŀ", "l"));
				list.add(new Translate("ñ", "n"));
				list.add(new Translate("ń", "n"));
				list.add(new Translate("ň", "n"));
				list.add(new Translate("ņ", "n"));
				list.add(new Translate("ŉ", "n"));
				list.add(new Translate("ŋ", "n"));
				list.add(new Translate("ò", "o"));
				list.add(new Translate("ó", "o"));
				list.add(new Translate("ô", "o"));
				list.add(new Translate("õ", "o"));
				list.add(new Translate("ø", "o"));
				list.add(new Translate("ō", "o"));
				list.add(new Translate("ő", "o"));
				list.add(new Translate("ŏ", "o"));
				list.add(new Translate("ŕ", "r"));
				list.add(new Translate("ř", "r"));
				list.add(new Translate("ŗ", "r"));
				list.add(new Translate("ś", "s"));
				list.add(new Translate("š", "s"));
				list.add(new Translate("ť", "t"));
				list.add(new Translate("ù", "u"));
				list.add(new Translate("ú", "u"));
				list.add(new Translate("û", "u"));
				list.add(new Translate("ū", "u"));
				list.add(new Translate("ů", "u"));
				list.add(new Translate("ű", "u"));
				list.add(new Translate("ŭ", "u"));
				list.add(new Translate("ũ", "u"));
				list.add(new Translate("ų", "u"));
				list.add(new Translate("ŵ", "w"));
				list.add(new Translate("ÿ", "y"));
				list.add(new Translate("ý", "y"));
				list.add(new Translate("ŷ", "y"));
				list.add(new Translate("ż", "z"));
				list.add(new Translate("ź", "z"));
				list.add(new Translate("ž", "z"));
				list.add(new Translate("Α", "A"));
				list.add(new Translate("Ά", "A"));
				list.add(new Translate("Ἀ", "A"));
				list.add(new Translate("Ἁ", "A"));
				list.add(new Translate("Ἂ", "A"));
				list.add(new Translate("Ἃ", "A"));
				list.add(new Translate("Ἄ", "A"));
				list.add(new Translate("Ἅ", "A"));
				list.add(new Translate("Ἆ", "A"));
				list.add(new Translate("Ἇ", "A"));
				list.add(new Translate("ᾈ", "A"));
				list.add(new Translate("ᾉ", "A"));
				list.add(new Translate("ᾊ", "A"));
				list.add(new Translate("ᾋ", "A"));
				list.add(new Translate("ᾌ", "A"));
				list.add(new Translate("ᾍ", "A"));
				list.add(new Translate("ᾎ", "A"));
				list.add(new Translate("ᾏ", "A"));
				list.add(new Translate("Ᾰ", "A"));
				list.add(new Translate("Ᾱ", "A"));
				list.add(new Translate("Ὰ", "A"));
				list.add(new Translate("ᾼ", "A"));
				list.add(new Translate("Β", "B"));
				list.add(new Translate("Γ", "G"));
				list.add(new Translate("Δ", "D"));
				list.add(new Translate("Ε", "E"));
				list.add(new Translate("Έ", "E"));
				list.add(new Translate("Ἐ", "E"));
				list.add(new Translate("Ἑ", "E"));
				list.add(new Translate("Ἒ", "E"));
				list.add(new Translate("Ἓ", "E"));
				list.add(new Translate("Ἔ", "E"));
				list.add(new Translate("Ἕ", "E"));
				list.add(new Translate("Ὲ", "E"));
				list.add(new Translate("Ζ", "Z"));
				list.add(new Translate("Η", "I"));
				list.add(new Translate("Ή", "I"));
				list.add(new Translate("Ἠ", "I"));
				list.add(new Translate("Ἡ", "I"));
				list.add(new Translate("Ἢ", "I"));
				list.add(new Translate("Ἣ", "I"));
				list.add(new Translate("Ἤ", "I"));
				list.add(new Translate("Ἥ", "I"));
				list.add(new Translate("Ἦ", "I"));
				list.add(new Translate("Ἧ", "I"));
				list.add(new Translate("ᾘ", "I"));
				list.add(new Translate("ᾙ", "I"));
				list.add(new Translate("ᾚ", "I"));
				list.add(new Translate("ᾛ", "I"));
				list.add(new Translate("ᾜ", "I"));
				list.add(new Translate("ᾝ", "I"));
				list.add(new Translate("ᾞ", "I"));
				list.add(new Translate("ᾟ", "I"));
				list.add(new Translate("Ὴ", "I"));
				list.add(new Translate("ῌ", "I"));
				list.add(new Translate("Θ", "T"));
				list.add(new Translate("Ι", "I"));
				list.add(new Translate("Ί", "I"));
				list.add(new Translate("Ϊ", "I"));
				list.add(new Translate("Ἰ", "I"));
				list.add(new Translate("Ἱ", "I"));
				list.add(new Translate("Ἲ", "I"));
				list.add(new Translate("Ἳ", "I"));
				list.add(new Translate("Ἴ", "I"));
				list.add(new Translate("Ἵ", "I"));
				list.add(new Translate("Ἶ", "I"));
				list.add(new Translate("Ἷ", "I"));
				list.add(new Translate("Ῐ", "I"));
				list.add(new Translate("Ῑ", "I"));
				list.add(new Translate("Ὶ", "I"));
				list.add(new Translate("Κ", "K"));
				list.add(new Translate("Λ", "L"));
				list.add(new Translate("Μ", "M"));
				list.add(new Translate("Ν", "N"));
				list.add(new Translate("Ξ", "K"));
				list.add(new Translate("Ο", "O"));
				list.add(new Translate("Ό", "O"));
				list.add(new Translate("Ὀ", "O"));
				list.add(new Translate("Ὁ", "O"));
				list.add(new Translate("Ὂ", "O"));
				list.add(new Translate("Ὃ", "O"));
				list.add(new Translate("Ὄ", "O"));
				list.add(new Translate("Ὅ", "O"));
				list.add(new Translate("Ὸ", "O"));
				list.add(new Translate("Π", "P"));
				list.add(new Translate("Ρ", "R"));
				list.add(new Translate("Ῥ", "R"));
				list.add(new Translate("Σ", "S"));
				list.add(new Translate("Τ", "T"));
				list.add(new Translate("Υ", "Y"));
				list.add(new Translate("Ύ", "Y"));
				list.add(new Translate("Ϋ", "Y"));
				list.add(new Translate("Ὑ", "Y"));
				list.add(new Translate("Ὓ", "Y"));
				list.add(new Translate("Ὕ", "Y"));
				list.add(new Translate("Ὗ", "Y"));
				list.add(new Translate("Ῠ", "Y"));
				list.add(new Translate("Ῡ", "Y"));
				list.add(new Translate("Ὺ", "Y"));
				list.add(new Translate("Φ", "F"));
				list.add(new Translate("Χ", "X"));
				list.add(new Translate("Ψ", "P"));
				list.add(new Translate("Ω", "O"));
				list.add(new Translate("Ώ", "O"));
				list.add(new Translate("Ὠ", "O"));
				list.add(new Translate("Ὡ", "O"));
				list.add(new Translate("Ὢ", "O"));
				list.add(new Translate("Ὣ", "O"));
				list.add(new Translate("Ὤ", "O"));
				list.add(new Translate("Ὥ", "O"));
				list.add(new Translate("Ὦ", "O"));
				list.add(new Translate("Ὧ", "O"));
				list.add(new Translate("ᾨ", "O"));
				list.add(new Translate("ᾩ", "O"));
				list.add(new Translate("ᾪ", "O"));
				list.add(new Translate("ᾫ", "O"));
				list.add(new Translate("ᾬ", "O"));
				list.add(new Translate("ᾭ", "O"));
				list.add(new Translate("ᾮ", "O"));
				list.add(new Translate("ᾯ", "O"));
				list.add(new Translate("Ὼ", "O"));
				list.add(new Translate("ῼ", "O"));
				list.add(new Translate("α", "a"));
				list.add(new Translate("ά", "a"));
				list.add(new Translate("ἀ", "a"));
				list.add(new Translate("ἁ", "a"));
				list.add(new Translate("ἂ", "a"));
				list.add(new Translate("ἃ", "a"));
				list.add(new Translate("ἄ", "a"));
				list.add(new Translate("ἅ", "a"));
				list.add(new Translate("ἆ", "a"));
				list.add(new Translate("ἇ", "a"));
				list.add(new Translate("ᾀ", "a"));
				list.add(new Translate("ᾁ", "a"));
				list.add(new Translate("ᾂ", "a"));
				list.add(new Translate("ᾃ", "a"));
				list.add(new Translate("ᾄ", "a"));
				list.add(new Translate("ᾅ", "a"));
				list.add(new Translate("ᾆ", "a"));
				list.add(new Translate("ᾇ", "a"));
				list.add(new Translate("ὰ", "a"));
				list.add(new Translate("ᾰ", "a"));
				list.add(new Translate("ᾱ", "a"));
				list.add(new Translate("ᾲ", "a"));
				list.add(new Translate("ᾳ", "a"));
				list.add(new Translate("ᾴ", "a"));
				list.add(new Translate("ᾶ", "a"));
				list.add(new Translate("ᾷ", "a"));
				list.add(new Translate("β", "b"));
				list.add(new Translate("γ", "g"));
				list.add(new Translate("δ", "d"));
				list.add(new Translate("ε", "e"));
				list.add(new Translate("έ", "e"));
				list.add(new Translate("ἐ", "e"));
				list.add(new Translate("ἑ", "e"));
				list.add(new Translate("ἒ", "e"));
				list.add(new Translate("ἓ", "e"));
				list.add(new Translate("ἔ", "e"));
				list.add(new Translate("ἕ", "e"));
				list.add(new Translate("ὲ", "e"));
				list.add(new Translate("ζ", "z"));
				list.add(new Translate("η", "i"));
				list.add(new Translate("ή", "i"));
				list.add(new Translate("ἠ", "i"));
				list.add(new Translate("ἡ", "i"));
				list.add(new Translate("ἢ", "i"));
				list.add(new Translate("ἣ", "i"));
				list.add(new Translate("ἤ", "i"));
				list.add(new Translate("ἥ", "i"));
				list.add(new Translate("ἦ", "i"));
				list.add(new Translate("ἧ", "i"));
				list.add(new Translate("ᾐ", "i"));
				list.add(new Translate("ᾑ", "i"));
				list.add(new Translate("ᾒ", "i"));
				list.add(new Translate("ᾓ", "i"));
				list.add(new Translate("ᾔ", "i"));
				list.add(new Translate("ᾕ", "i"));
				list.add(new Translate("ᾖ", "i"));
				list.add(new Translate("ᾗ", "i"));
				list.add(new Translate("ὴ", "i"));
				list.add(new Translate("ῂ", "i"));
				list.add(new Translate("ῃ", "i"));
				list.add(new Translate("ῄ", "i"));
				list.add(new Translate("ῆ", "i"));
				list.add(new Translate("ῇ", "i"));
				list.add(new Translate("θ", "t"));
				list.add(new Translate("ι", "i"));
				list.add(new Translate("ί", "i"));
				list.add(new Translate("ϊ", "i"));
				list.add(new Translate("ΐ", "i"));
				list.add(new Translate("ἰ", "i"));
				list.add(new Translate("ἱ", "i"));
				list.add(new Translate("ἲ", "i"));
				list.add(new Translate("ἳ", "i"));
				list.add(new Translate("ἴ", "i"));
				list.add(new Translate("ἵ", "i"));
				list.add(new Translate("ἶ", "i"));
				list.add(new Translate("ἷ", "i"));
				list.add(new Translate("ὶ", "i"));
				list.add(new Translate("ῐ", "i"));
				list.add(new Translate("ῑ", "i"));
				list.add(new Translate("ῒ", "i"));
				list.add(new Translate("ῖ", "i"));
				list.add(new Translate("ῗ", "i"));
				list.add(new Translate("κ", "k"));
				list.add(new Translate("λ", "l"));
				list.add(new Translate("μ", "m"));
				list.add(new Translate("ν", "n"));
				list.add(new Translate("ξ", "k"));
				list.add(new Translate("ο", "o"));
				list.add(new Translate("ό", "o"));
				list.add(new Translate("ὀ", "o"));
				list.add(new Translate("ὁ", "o"));
				list.add(new Translate("ὂ", "o"));
				list.add(new Translate("ὃ", "o"));
				list.add(new Translate("ὄ", "o"));
				list.add(new Translate("ὅ", "o"));
				list.add(new Translate("ὸ", "o"));
				list.add(new Translate("π", "p"));
				list.add(new Translate("ρ", "r"));
				list.add(new Translate("ῤ", "r"));
				list.add(new Translate("ῥ", "r"));
				list.add(new Translate("σ", "s"));
				list.add(new Translate("ς", "s"));
				list.add(new Translate("τ", "t"));
				list.add(new Translate("υ", "y"));
				list.add(new Translate("ύ", "y"));
				list.add(new Translate("ϋ", "y"));
				list.add(new Translate("ΰ", "y"));
				list.add(new Translate("ὐ", "y"));
				list.add(new Translate("ὑ", "y"));
				list.add(new Translate("ὒ", "y"));
				list.add(new Translate("ὓ", "y"));
				list.add(new Translate("ὔ", "y"));
				list.add(new Translate("ὕ", "y"));
				list.add(new Translate("ὖ", "y"));
				list.add(new Translate("ὗ", "y"));
				list.add(new Translate("ὺ", "y"));
				list.add(new Translate("ῠ", "y"));
				list.add(new Translate("ῡ", "y"));
				list.add(new Translate("ῢ", "y"));
				list.add(new Translate("ῦ", "y"));
				list.add(new Translate("ῧ", "y"));
				list.add(new Translate("φ", "f"));
				list.add(new Translate("χ", "x"));
				list.add(new Translate("ψ", "p"));
				list.add(new Translate("ω", "o"));
				list.add(new Translate("ώ", "o"));
				list.add(new Translate("ὠ", "o"));
				list.add(new Translate("ὡ", "o"));
				list.add(new Translate("ὢ", "o"));
				list.add(new Translate("ὣ", "o"));
				list.add(new Translate("ὤ", "o"));
				list.add(new Translate("ὥ", "o"));
				list.add(new Translate("ὦ", "o"));
				list.add(new Translate("ὧ", "o"));
				list.add(new Translate("ᾠ", "o"));
				list.add(new Translate("ᾡ", "o"));
				list.add(new Translate("ᾢ", "o"));
				list.add(new Translate("ᾣ", "o"));
				list.add(new Translate("ᾤ", "o"));
				list.add(new Translate("ᾥ", "o"));
				list.add(new Translate("ᾦ", "o"));
				list.add(new Translate("ᾧ", "o"));
				list.add(new Translate("ὼ", "o"));
				list.add(new Translate("ῲ", "o"));
				list.add(new Translate("ῳ", "o"));
				list.add(new Translate("ῴ", "o"));
				list.add(new Translate("ῶ", "o"));
				list.add(new Translate("ῷ", "o"));
				list.add(new Translate("А", "A"));
				list.add(new Translate("Б", "B"));
				list.add(new Translate("В", "V"));
				list.add(new Translate("Г", "G"));
				list.add(new Translate("Д", "D"));
				list.add(new Translate("Е", "E"));
				list.add(new Translate("Ё", "E"));
				list.add(new Translate("Ж", "Z"));
				list.add(new Translate("З", "Z"));
				list.add(new Translate("И", "I"));
				list.add(new Translate("Й", "I"));
				list.add(new Translate("К", "K"));
				list.add(new Translate("Л", "L"));
				list.add(new Translate("М", "M"));
				list.add(new Translate("Н", "N"));
				list.add(new Translate("О", "O"));
				list.add(new Translate("П", "P"));
				list.add(new Translate("Р", "R"));
				list.add(new Translate("С", "S"));
				list.add(new Translate("Т", "T"));
				list.add(new Translate("У", "U"));
				list.add(new Translate("Ф", "F"));
				list.add(new Translate("Х", "K"));
				list.add(new Translate("Ц", "T"));
				list.add(new Translate("Ч", "C"));
				list.add(new Translate("Ш", "S"));
				list.add(new Translate("Щ", "S"));
				list.add(new Translate("Ы", "Y"));
				list.add(new Translate("Э", "E"));
				list.add(new Translate("Ю", "Y"));
				list.add(new Translate("Я", "Y"));
				list.add(new Translate("а", "A"));
				list.add(new Translate("б", "B"));
				list.add(new Translate("в", "V"));
				list.add(new Translate("г", "G"));
				list.add(new Translate("д", "D"));
				list.add(new Translate("е", "E"));
				list.add(new Translate("ё", "E"));
				list.add(new Translate("ж", "Z"));
				list.add(new Translate("з", "Z"));
				list.add(new Translate("и", "I"));
				list.add(new Translate("й", "I"));
				list.add(new Translate("к", "K"));
				list.add(new Translate("л", "L"));
				list.add(new Translate("м", "M"));
				list.add(new Translate("н", "N"));
				list.add(new Translate("о", "O"));
				list.add(new Translate("п", "P"));
				list.add(new Translate("р", "R"));
				list.add(new Translate("с", "S"));
				list.add(new Translate("т", "T"));
				list.add(new Translate("у", "U"));
				list.add(new Translate("ф", "F"));
				list.add(new Translate("х", "K"));
				list.add(new Translate("ц", "T"));
				list.add(new Translate("ч", "C"));
				list.add(new Translate("ш", "S"));
				list.add(new Translate("щ", "S"));
				list.add(new Translate("ы", "Y"));
				list.add(new Translate("э", "E"));
				list.add(new Translate("ю", "Y"));
				list.add(new Translate("я", "Y"));
				list.add(new Translate("ð", "d"));
				list.add(new Translate("Ð", "D"));
				list.add(new Translate("þ", "t"));
				list.add(new Translate("Þ", "T"));
				list.add(new Translate("ა", "a"));
				list.add(new Translate("ბ", "b"));
				list.add(new Translate("გ", "g"));
				list.add(new Translate("დ", "d"));
				list.add(new Translate("ე", "e"));
				list.add(new Translate("ვ", "v"));
				list.add(new Translate("ზ", "z"));
				list.add(new Translate("თ", "t"));
				list.add(new Translate("ი", "i"));
				list.add(new Translate("კ", "k"));
				list.add(new Translate("ლ", "l"));
				list.add(new Translate("მ", "m"));
				list.add(new Translate("ნ", "n"));
				list.add(new Translate("ო", "o"));
				list.add(new Translate("პ", "p"));
				list.add(new Translate("ჟ", "z"));
				list.add(new Translate("რ", "r"));
				list.add(new Translate("ს", "s"));
				list.add(new Translate("ტ", "t"));
				list.add(new Translate("უ", "u"));
				list.add(new Translate("ფ", "p"));
				list.add(new Translate("ქ", "k"));
				list.add(new Translate("ღ", "g"));
				list.add(new Translate("ყ", "q"));
				list.add(new Translate("შ", "s"));
				list.add(new Translate("ჩ", "c"));
				list.add(new Translate("ც", "t"));
				list.add(new Translate("ძ", "d"));
				list.add(new Translate("წ", "t"));
				list.add(new Translate("ჭ", "c"));
				list.add(new Translate("ხ", "k"));
				list.add(new Translate("ჯ", "j"));
				list.add(new Translate("ჰ", "h"));
				list.add(new Translate("’", "-"));
				list.add(new Translate("'", "-"));
				list.add(new Translate("\\", "-"));
			list.add(new Translate(" ", "-"));
			return list;
		}
	}

	public static String removeSpecialChar(String str) {
		List<Translate> translates = new Translate().iteration();
		for (Translate t : translates) {
			String iten = t.getItem();
			try {
				if (str.contains(iten)) {
					str = str.replaceAll(t.getItem(), t.getItemTrans());
				}
			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		log.info("removeSpecialChar result = " + str);
		return str;
	}

}
