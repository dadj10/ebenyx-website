package com.ebenyx.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ebenyx.entities.Role;

public interface RoleRepository extends JpaRepository<Role, String> {

	@Query("SELECT c FROM Role c WHERE c.role = ?1 AND c.etat = 1")
	Role findRole(String nomRole);

}
