package com.ebenyx.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ebenyx.entities.ClientEbenyx;

public interface ClientEbenyxRepository extends JpaRepository<ClientEbenyx, Long>{
	
	@Query("SELECT c FROM ClientEbenyx c WHERE lower(c.raisonSociale) = ?1 AND c.enabled = true")
	ClientEbenyx findItemByName(String nom);
	
	@Query("SELECT c FROM ClientEbenyx c WHERE c.etat = 1 AND c.enabled = true ORDER BY c.idClientEbenyx DESC")
	List<ClientEbenyx> findAllItems();
	
	@Query("SELECT c FROM ClientEbenyx c WHERE c.idClientEbenyx = ?1 AND c.enabled = true")
	ClientEbenyx findItemById(Long id);

}
