package com.ebenyx.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ebenyx.entities.Expertise;

public interface ExpertiseRepository extends JpaRepository<Expertise, Long> {

}
