package com.ebenyx.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ebenyx.entities.Partenaire;

public interface PartenaireRepository extends JpaRepository<Partenaire, Long> {

	@Query("SELECT c FROM Partenaire c WHERE lower(c.nom) = ?1 AND c.enabled = true")
	Partenaire findItemByName(String nom);

	@Query("SELECT c FROM Partenaire c WHERE c.etat = 1 AND c.enabled = true ORDER BY c.idPartenaire DESC")
	List<Partenaire> findAllItems();

	@Query("SELECT c FROM Partenaire c WHERE c.idPartenaire = ?1 AND c.enabled = true")
	Partenaire findItemById(Long id);

}
