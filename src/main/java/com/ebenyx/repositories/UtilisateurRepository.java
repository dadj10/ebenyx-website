package com.ebenyx.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ebenyx.entities.Utilisateur;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {
	
	@Query("SELECT c FROM Utilisateur c WHERE lower(c.username) = ?1")
	Utilisateur findUtilisateurByUsername(String username);
	
	@Query("SELECT c FROM Utilisateur c WHERE lower(c.username) = ?1 AND c.etat = 1")
	Utilisateur findUtilisateurActifByUsername(String username);
	
	@Query("SELECT c FROM Utilisateur c WHERE lower(c.username) = ?1 AND c.etat = 0")
	Utilisateur findUtilisateurInActifByUsername(String username);

}
