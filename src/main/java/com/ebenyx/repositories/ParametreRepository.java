package com.ebenyx.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ebenyx.entities.Parametre;

public interface ParametreRepository extends JpaRepository<Parametre, Long> {

	@Query(value = "SELECT * from parametre limit 1", nativeQuery = true)
	Parametre get();

	@Query(value = "SELECT photo_defaut FROM parametre limit 1", nativeQuery = true)
	String findPhotoDefaut();

	@Query(value = "SELECT longeur_resumer_article FROM parametre limit 1", nativeQuery = true)
	Long getLongeurResumerArticle();
	
	@Query(value = "SELECT blog_image_defaut FROM parametre limit 1", nativeQuery = true)
	String findBlogImageDefaut();
	
	@Query(value = "SELECT solution_image_defaut FROM parametre limit 1", nativeQuery = true)
	String findSolutionImageDefaut();
	
	@Query(value = "SELECT certification_image_defaut FROM parametre limit 1", nativeQuery = true)
	String findCertificatDefaut();
	
	@Query(value = "SELECT partenaire_image_defaut FROM parametre limit 1", nativeQuery = true)
	String findPartenaireDefaut();

}