package com.ebenyx.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ebenyx.entities.Actualite;
import com.ebenyx.entities.ActualiteStatus;

public interface ActualiteRepository extends JpaRepository<Actualite, Long>{
	
	@Query("SELECT c FROM Actualite c WHERE c.etat = 1 ORDER BY c.idActualite DESC")
	List<Actualite> findAllActualiteOrderByIdDesc();
	
	@Query("SELECT c FROM Actualite c WHERE DATE(c.dateDeParution) = CURRENT_DATE AND c.idActualiteStatus = ?1 AND c.etat = 1 ORDER BY c.idActualite DESC")
	List<Actualite> findActuDuJour(ActualiteStatus actualiteStatus);
	
	@Query("SELECT c FROM Actualite c WHERE c.idActualiteStatus = ?1 AND c.etat = 1 AND enabled = true ORDER BY c.idActualite DESC")
	List<Actualite> findActuByStatus(ActualiteStatus actualiteStatus);

	@Query("SELECT c FROM Actualite c WHERE DATE(c.dateDeParution) <= CURRENT_DATE AND c.idActualiteStatus = ?1 AND c.etat = 1 ORDER BY c.idActualite DESC")
	List<Actualite> findActuOfBlog(ActualiteStatus actualiteStatus);
	
	// @Query("SELECT c FROM Actualite c WHERE DATE(c.dateDeParution) <= CURRENT_DATE AND c.idActualiteStatus = ?1 AND c.etat = 1 ORDER BY c.idActualite DESC")
	// List<Actualite> findLast3ActuOfBlog(ActualiteStatus actualiteStatus);
	
	@Query(value = "SELECT * FROM actualite",
			nativeQuery = true)
	Page<Actualite> findLastsActuOfBlog(Pageable pageable, Long idActualiteStatus);
	
	@Query(value = "SELECT * FROM actualite WHERE id_actualite_status = ?1 AND DATE(date_de_parution) <= CURRENT_DATE AND etat = 1 ORDER BY id_actualite DESC LIMIT 3",
			nativeQuery = true)
	List<Actualite> findLasts3ActuOfBlog(Long idActualiteStatus);

	@Query("SELECT c FROM Actualite c WHERE c.referenceActualite = ?1 AND c.idActualiteStatus = ?2 AND c.etat = 1")
	Actualite findActualiteByReferenceAndStatus(String referenceActualite, ActualiteStatus published);
	
	@Query("SELECT c FROM Actualite c WHERE c.soeUrl = ?1 AND c.idActualiteStatus = ?2 AND c.etat = 1")
	Actualite findActualiteBySoeUrlAndStatus(String soeUrl, ActualiteStatus published);
	
	@Query("SELECT c FROM Actualite c WHERE c.idActualiteStatus = ?1 AND c.etat = 1")
	Actualite findActualiteByStatus(ActualiteStatus actualiteStatus);
	
	@Query("SELECT c FROM Actualite c WHERE c.idActualiteStatus = ?1 AND c.etat = 1")
	List<Actualite> findAllActualiteByStatus(ActualiteStatus actualiteStatus);
	
	@Query("SELECT c FROM Actualite c WHERE c.idActualite <> ?1 AND c.idActualiteStatus = ?2 AND c.etat = 1")
	List<Actualite> findResteActualiteByStatus(Long idActualite, ActualiteStatus published);

	@Query("SELECT c FROM Actualite c WHERE c.idActualite = ?1 AND c.idActualiteStatus = ?2 AND c.etat = 1")
	Actualite findBlogNav(long idActualite, ActualiteStatus published);
	
	// @Query("SELECT COUNT(c) FROM Product c WHERE c.idActualiteStatus = ?1 AND c.etat = 1")
	// long countItemByStatus(ActualiteStatus status);

}
