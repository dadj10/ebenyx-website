package com.ebenyx.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ebenyx.entities.AbonnesNewsletters;

public interface AbonnesNewslettersRepository extends JpaRepository<AbonnesNewsletters, Long>{
	
	@Query("SELECT c FROM AbonnesNewsletters c WHERE lower(c.email) = ?1 AND enabled = true")
	AbonnesNewsletters findAbonnesNewsletters(String email);

}
