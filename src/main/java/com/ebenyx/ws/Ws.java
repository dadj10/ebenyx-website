package com.ebenyx.ws;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ebenyx.config.Utils;
import com.ebenyx.entities.AbonnesNewsletters;
import com.ebenyx.entities.ContactMessage;
import com.ebenyx.entities.Sujet;
import com.ebenyx.repositories.AbonnesNewslettersRepository;
import com.ebenyx.repositories.ContactMessageRepository;
import com.ebenyx.repositories.SujetRepository;
import com.ebenyx.ws.reponse.ResponseService;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/ws")
public class Ws {

	private static final Logger log = LoggerFactory.getLogger(Ws.class);
	
	@Autowired
	public AbonnesNewslettersRepository abonnesNewslettersRepos;
	@Autowired
	public SujetRepository sujetRepos;
	@Autowired
	public ContactMessageRepository contactMessageRepos;

	@Value("${server.address}")
	private String addressServer;

	@Value("${server.port}")
	private String portServer;

	// ========== Ajax Controllers ========= //
	
	/**
	 * Cette fonction permet a une particulier de poster a une offre
	 */
	@GetMapping(value = "/subscribe_newsletters/{email}")
	public ResponseEntity<Object> subscribeNewsLetters(@PathVariable("email") String email) {
		AbonnesNewsletters retour = null;
		try {
			email = email.trim();
			// Je verifie que le parametre transmi est correct
			if (email.length() != 0 && Utils.isValidEmailAddress(email)) {
				
				// Je verifie que l'adresse mail n'est pas deja pas en base
				retour = abonnesNewslettersRepos.findAbonnesNewsletters(email);
				
				if (retour != null) {
					String message = "Vous êtes déja abonnnés à la newsletters Ebenyx";

					ResponseService<AbonnesNewsletters> response = new ResponseService<AbonnesNewsletters>();
					response.setStatus(0);
					response.setMessage(message);
					response.setData(null);
					return new ResponseEntity<Object>(response, HttpStatus.OK);
				}
				
				AbonnesNewsletters abonne = new AbonnesNewsletters();
				
				abonne.setNom(Utils.getUsernameFromEmail(email));
				abonne.setEmail(email);
				abonne.setEtat(1);
				
				abonne = abonnesNewslettersRepos.save(abonne);
				
				// J'envois une motification a l'entreprise
				// service.afterSubscribeNewsLetters(abonne, addressServer, portServer, aEnvoyerRepository);

				String message = "Votre abonnement à la newsletters a bien été soumise, vous recevrai desormais dernières actualités Ebenyx Technologies ainsi que les innovations technologiques directement dans votre messagerie.";

				ResponseService<AbonnesNewsletters> response = new ResponseService<AbonnesNewsletters>();
				response.setStatus(1);
				response.setMessage(message);
				response.setData(null);
				return new ResponseEntity<Object>(response, HttpStatus.OK);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		
		return null;
	}
	
	/**
	 * Cette fonction permet d'enregistrer les messages du formulaire de contact.
	 */
	@PostMapping(value = "/send_msg_contact")
	public ResponseEntity<Object> contact(@RequestBody ContactMessage o) {
		try {
			/*
			System.out.println("idSujet = "+ o.getIdSujet().getNom());
			System.out.println("nomComplet = " + o.getNomComplet());
			System.out.println("email = "+ o.getEmail());
			System.out.println("telephone = " + o.getTelephone());
			System.out.println("message = " + o.getMessage());
			*/
			
			// Je verifie que le sujet existe
			Sujet sujet = null;
			sujet = sujetRepos.findItemById(Long.parseLong(o.getIdSujet().getNom()));

			if (sujet == null) {
				String message = "Veuillez renseigner correctement le formulaire";

				ResponseService<ContactMessage> response = new ResponseService<ContactMessage>();
				response.setStatus(0);
				response.setMessage(message);
				response.setData(null);
				return new ResponseEntity<Object>(response, HttpStatus.OK);
			}

			o.setIdSujet(sujet);
			o.setDateCreation(new Date());

			contactMessageRepos.save(o);

			String message = "Votre requète a bien été soumise, l'équipe support vous contactera sous peu. Ebenyx Technologies vous remercie.";

			ResponseService<ContactMessage> response = new ResponseService<ContactMessage>();
			response.setStatus(1);
			response.setMessage(message);
			response.setData(o);
			return new ResponseEntity<Object>(response, HttpStatus.OK);
			
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		
		return null;
	}
}
