package com.ebenyx.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ebenyx.config.Constantes;

@Entity
@Table(name = "parametre")
public class Parametre implements Serializable {

	private static final long serialVersionUID = 1L;

	// Attributs
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String applicationName;

	// Merssagerie
	private String mailHost;
	private String mailUsername;
	private String mailSender;
	private String mailPassword;
	private String mailPort;
	private String mailProtocol;

	private boolean mailSmtpSslEnable; // ssl.enable
	private boolean mailSmtpAuth; // auth
	private boolean mailSmtpStarttlsEnable;

	private boolean mailPropertiesMailDebug; // mail.debug
	private boolean mailSessionMailDebug; // session.setDebug

	// config url
	private String baseUrl; // host:port
	private String protocol;

	// Email system
	private String emailSystem;

	// donnees Ebenyx
	private String pageFacebook;
	private String pageTwitter;
	private String pageLinkedIn;
	private String pageYoutube;
	private String whatsApp;

	private String localisation;

	@Column(length = 5000)
	private String geoLocalisation;

	private String horaireOuverture;

	private String emailContact;
	private String emailSupport;

	private String contactInfoLine_1;
	private String contactInfoLine_2;
	private String contactInfoLine_3;

	private String photoDefaut = null;

	// Images
	private String blogImageDefaut = null;
	private String clientImageDefaut = null;
	private String solutionImageDefaut = null;
	private String realisationImageDefaut = null;
	private String partenaireImageDefaut = null;
	private String certificationImageDefaut = null;
	private String termeEtConditions = null;

	private String fondateur;
	private String motFondateur;
	private String brochure;

	// Parametre article
	private Long longeurResumerArticle; // longeur du resumé d'un article

	public Parametre() {
		super();
	}

	public Parametre(String applicationName, String mailHost, String mailUsername, String mailSender,
			String mailPassword, String mailPort, String mailProtocol, boolean mailSmtpSslEnable, boolean mailSmtpAuth,
			boolean mailSmtpStarttlsEnable, boolean mailPropertiesMailDebug, boolean mailSessionMailDebug,
			String baseUrl, String protocol, String emailSystem, String pageFacebook, String pageTwitter,
			String pageLinkedIn, String whatsApp, String localisation, String geoLocalisation, String horaireOuverture,
			String emailContact, String emailSupport, String contactInfoLine_1, String contactInfoLine_2,
			String contactInfoLine_3, String photoDefaut, String blogImageDefaut, String termeEtConditions,
			String fondateur, String motFondateur, Long longeurResumerArticle, String brochure, String pageYoutube,
			String clientImageDefaut, String solutionImageDefaut, String realisationImageDefaut,
			String partenaireImageDefaut, String certificationImageDefaut) {
		super();
		this.applicationName = applicationName;
		this.mailHost = mailHost;
		this.mailUsername = mailUsername;
		this.mailSender = mailSender;
		this.mailPassword = mailPassword;
		this.mailPort = mailPort;
		this.mailProtocol = mailProtocol;
		this.mailSmtpSslEnable = mailSmtpSslEnable;
		this.mailSmtpAuth = mailSmtpAuth;
		this.mailSmtpStarttlsEnable = mailSmtpStarttlsEnable;
		this.mailPropertiesMailDebug = mailPropertiesMailDebug;
		this.mailSessionMailDebug = mailSessionMailDebug;
		this.baseUrl = baseUrl;
		this.protocol = protocol;
		this.emailSystem = emailSystem;
		this.pageFacebook = pageFacebook;
		this.pageTwitter = pageTwitter;
		this.pageLinkedIn = pageLinkedIn;
		this.whatsApp = whatsApp;
		this.localisation = localisation;
		this.geoLocalisation = geoLocalisation;
		this.horaireOuverture = horaireOuverture;
		this.emailContact = emailContact;
		this.emailSupport = emailSupport;
		this.contactInfoLine_1 = contactInfoLine_1;
		this.contactInfoLine_2 = contactInfoLine_2;
		this.contactInfoLine_3 = contactInfoLine_3;
		this.photoDefaut = photoDefaut;
		this.blogImageDefaut = blogImageDefaut;
		this.termeEtConditions = termeEtConditions;
		this.longeurResumerArticle = longeurResumerArticle;
		this.fondateur = fondateur;
		this.motFondateur = motFondateur;
		this.brochure = brochure;
		this.pageYoutube = pageYoutube;
		this.clientImageDefaut = clientImageDefaut;
		this.solutionImageDefaut = solutionImageDefaut;
		this.realisationImageDefaut = realisationImageDefaut;
		this.partenaireImageDefaut = partenaireImageDefaut;
		this.certificationImageDefaut = certificationImageDefaut;
	}

	public String getGeoLocalisation() {
		return geoLocalisation;
	}

	public void setGeoLocalisation(String geoLocalisation) {
		this.geoLocalisation = geoLocalisation;
	}

	public String getHoraireOuverture() {
		return horaireOuverture;
	}

	public void setHoraireOuverture(String horaireOuverture) {
		this.horaireOuverture = horaireOuverture;
	}

	public String getEmailContact() {
		return emailContact;
	}

	public void setEmailContact(String emailContact) {
		this.emailContact = emailContact;
	}

	public String getEmailSupport() {
		return emailSupport;
	}

	public void setEmailSupport(String emailSupport) {
		this.emailSupport = emailSupport;
	}

	public String getContactInfoLine_1() {
		return contactInfoLine_1;
	}

	public void setContactInfoLine_1(String contactInfoLine_1) {
		this.contactInfoLine_1 = contactInfoLine_1;
	}

	public String getContactInfoLine_2() {
		return contactInfoLine_2;
	}

	public void setContactInfoLine_2(String contactInfoLine_2) {
		this.contactInfoLine_2 = contactInfoLine_2;
	}

	public String getContactInfoLine_3() {
		return contactInfoLine_3;
	}

	public void setContactInfoLine_3(String contactInfoLine_3) {
		this.contactInfoLine_3 = contactInfoLine_3;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMailHost() {
		return mailHost;
	}

	public void setMailHost(String mailHost) {
		this.mailHost = mailHost;
	}

	public String getMailUsername() {
		return mailUsername;
	}

	public void setMailUsername(String mailUsername) {
		this.mailUsername = mailUsername;
	}

	public String getMailSender() {
		return mailSender;
	}

	public void setMailSender(String mailSender) {
		this.mailSender = mailSender;
	}

	public String getMailPassword() {
		return mailPassword;
	}

	public void setMailPassword(String mailPassword) {
		this.mailPassword = mailPassword;
	}

	public String getMailPort() {
		return mailPort;
	}

	public void setMailPort(String mailPort) {
		this.mailPort = mailPort;
	}

	public String getMailProtocol() {
		return mailProtocol;
	}

	public void setMailProtocol(String mailProtocol) {
		this.mailProtocol = mailProtocol;
	}

	public boolean isMailSmtpSslEnable() {
		return mailSmtpSslEnable;
	}

	public void setMailSmtpSslEnable(boolean mailSmtpSslEnable) {
		this.mailSmtpSslEnable = mailSmtpSslEnable;
	}

	public boolean isMailSmtpAuth() {
		return mailSmtpAuth;
	}

	public void setMailSmtpAuth(boolean mailSmtpAuth) {
		this.mailSmtpAuth = mailSmtpAuth;
	}

	public boolean isMailSmtpStarttlsEnable() {
		return mailSmtpStarttlsEnable;
	}

	public void setMailSmtpStarttlsEnable(boolean mailSmtpStarttlsEnable) {
		this.mailSmtpStarttlsEnable = mailSmtpStarttlsEnable;
	}

	public boolean isMailPropertiesMailDebug() {
		return mailPropertiesMailDebug;
	}

	public void setMailPropertiesMailDebug(boolean mailPropertiesMailDebug) {
		this.mailPropertiesMailDebug = mailPropertiesMailDebug;
	}

	public boolean isMailSessionMailDebug() {
		return mailSessionMailDebug;
	}

	public void setMailSessionMailDebug(boolean mailSessionMailDebug) {
		this.mailSessionMailDebug = mailSessionMailDebug;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getEmailSystem() {
		return emailSystem;
	}

	public void setEmailSystem(String emailSystem) {
		this.emailSystem = emailSystem;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getPageFacebook() {
		return pageFacebook;
	}

	public void setPageFacebook(String pageFacebook) {
		this.pageFacebook = pageFacebook;
	}

	public String getPageTwitter() {
		return pageTwitter;
	}

	public void setPageTwitter(String pageTwitter) {
		this.pageTwitter = pageTwitter;
	}

	public String getPageLinkedIn() {
		return pageLinkedIn;
	}

	public void setPageLinkedIn(String pageLinkedIn) {
		this.pageLinkedIn = pageLinkedIn;
	}

	public String getLocalisation() {
		return localisation;
	}

	public void setLocalisation(String localisation) {
		this.localisation = localisation;
	}

	public String getPhotoDefaut() {
		return photoDefaut;
	}

	public void setPhotoDefaut(String photoDefaut) {
		this.photoDefaut = photoDefaut;
	}

	public String getTermeEtConditions() {
		return termeEtConditions;
	}

	public void setTermeEtConditions(String termeEtConditions) {
		this.termeEtConditions = termeEtConditions;
	}

	public Long getLongeurResumerArticle() {
		return longeurResumerArticle;
	}

	public void setLongeurResumerArticle(Long longeurResumerArticle) {
		this.longeurResumerArticle = longeurResumerArticle;
	}

	public String getBlogImageDefaut() {
		return blogImageDefaut;
	}

	public void setBlogImageDefaut(String blogImageDefaut) {
		this.blogImageDefaut = blogImageDefaut;
	}

	public String getWhatsApp() {
		return whatsApp;
	}

	public void setWhatsApp(String whatsApp) {
		this.whatsApp = whatsApp;
	}

	public String getFondateur() {
		return fondateur;
	}

	public void setFondateur(String fondateur) {
		this.fondateur = fondateur;
	}

	public String getMotFondateur() {
		return motFondateur;
	}

	public void setMotFondateur(String motFondateur) {
		this.motFondateur = motFondateur;
	}

	public String getBrochure() {
		return brochure;
	}

	public void setBrochure(String brochure) {
		this.brochure = brochure;
	}

	public String getPageYoutube() {
		return pageYoutube;
	}

	public void setPageYoutube(String pageYoutube) {
		this.pageYoutube = pageYoutube;
	}

	public String getSolutionImageDefaut() {
		return solutionImageDefaut;
	}

	public void setSolutionImageDefaut(String solutionImageDefaut) {
		this.solutionImageDefaut = solutionImageDefaut;
	}

	public String getRealisationImageDefaut() {
		return realisationImageDefaut;
	}

	public void setRealisationImageDefaut(String realisationImageDefaut) {
		this.realisationImageDefaut = realisationImageDefaut;
	}

	public String getClientImageDefaut() {
		return clientImageDefaut;
	}

	public void setClientImageDefaut(String clientImageDefaut) {
		this.clientImageDefaut = clientImageDefaut;
	}

	public String getPartenaireImageDefaut() {
		return partenaireImageDefaut;
	}

	public void setPartenaireImageDefaut(String partenaireImageDefaut) {
		this.partenaireImageDefaut = partenaireImageDefaut;
	}

	public String getCertificationImageDefaut() {
		return certificationImageDefaut;
	}

	public void setCertificationImageDefaut(String certificationImageDefaut) {
		this.certificationImageDefaut = certificationImageDefaut;
	}

	public Parametre get() {
		Parametre param = new Parametre();

		param.setApplicationName(Constantes.DEFAULT_SERVER_NAME);
		param.setMailHost(Constantes.DEFAULT_SMTP_HOST);
		param.setMailUsername(Constantes.DEFAULT_SMTP_EMAIL);
		param.setMailSender(Constantes.DEFAULT_SMTP_EMAIL);
		param.setMailPassword(Constantes.DEFAULT_SMTP_PASSWORD);
		param.setMailPort(Constantes.DEFAULT_SMTP_PORT);
		param.setMailProtocol("smtp");

		param.setMailSmtpSslEnable(Boolean.TRUE);
		param.setMailSmtpAuth(Boolean.TRUE);
		param.setMailSmtpStarttlsEnable(Boolean.TRUE);
		param.setMailPropertiesMailDebug(Boolean.TRUE);
		param.setMailSessionMailDebug(Boolean.TRUE);

		param.setProtocol(Constantes.PROTOCOL);

		param.setEmailSystem(Constantes.DEFAULT_ADMIN_SYSTM_EMAIL);

		// Lien sociaux.
		param.setPageFacebook(Constantes.PAGE_FACEBOOK);
		param.setPageLinkedIn(Constantes.PAGE_LINKEDIN);
		param.setPageTwitter(Constantes.PAGE_TWITTER);
		param.setPageYoutube(Constantes.PAGE_YOUTUBE);
		param.setWhatsApp(Constantes.NUM_WHATSAPP);

		param.setLocalisation(Constantes.LOCALISATION);
		param.setGeoLocalisation(Constantes.GEO_LOCALISATION);

		param.setEmailContact(Constantes.EMAIL_CONTACT_EBT);
		param.setEmailSupport("support@ebenyx.com");

		param.setContactInfoLine_1("(+225) 07 08 28 05 44");
		param.setContactInfoLine_2("(+225) 27 20 22 30 98");
		param.setContactInfoLine_3("(+225) 05 56 05 07 28");

		param.setLongeurResumerArticle(Constantes.LONGUEUR_RESUME_ARTICLE);

		param.setFondateur(Constantes.NOM_FONDATEEUR);
		param.setMotFondateur(Constantes.MOT_FONDATEUR);

		// Fichiers
		param.setPhotoDefaut(Constantes.DEFAULT_PHOTO);
		param.setBlogImageDefaut(Constantes.DEFAULT_BLOG_IMG);
		param.setSolutionImageDefaut(Constantes.DEFAUT_SOLUTION_IMG);
		param.setRealisationImageDefaut(Constantes.DEFAULT_REALISATION_IMG);
		param.setCertificationImageDefaut(Constantes.DEFAULT_CERTIFICATION_IMG);
		param.setPartenaireImageDefaut(Constantes.DEFAULT_PARTENAIRE_IMG);
		param.setBrochure(Constantes.DEFAULT_BROCHURE);

		return param;
	}

}
