package com.ebenyx.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.crypto.password.PasswordEncoder;

@Entity
@Table(name = "utilisateur")
public class Utilisateur implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@NotEmpty()
	private String username;
	private String password;
	private String passwordConfirm;

	private String nom;
	private String prenoms;
	@Email
	private String email;
	private String numeroTelephone;
	private String token;

	private int etat = 1;
	@DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private Date dateCreation = new Date();
	@DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private Date dateModification = new Date();

	@DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private Date dateMiseAjourPassword = new Date();
	private Integer statutMotdepasse;
	@DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private Date passwordUpdateDate;

	// Clés étrangères
	@ManyToOne
	@JoinColumn(name = "role", referencedColumnName = "role")
	private Role role;

	@ManyToOne
	@JoinColumn(name = "creerPar", referencedColumnName = "username")
	private Utilisateur creerPar;

	@ManyToOne
	@JoinColumn(name = "modifierPar", referencedColumnName = "username")
	private Utilisateur modifierPar;

	private Boolean newsLetters = false;
	private Boolean termeEtConditions = false;

	public Utilisateur() {
		super();
		updateToken();
		this.termeEtConditions = false;
	}

	public Utilisateur(@NotEmpty String username, String password, String passwordConfirm, String nom, String prenoms,
			@Email String email, String numeroTelephone, String token,
			int etat, Date dateCreation, Date dateModification, Date dateMiseAjourPassword, Integer statutMotdepasse,
			Date passwordUpdateDate, Boolean newsLetters, Boolean termeEtConditions) {
		super();
		this.username = username;
		this.password = password;
		this.passwordConfirm = passwordConfirm;
		this.nom = nom;
		this.prenoms = prenoms;
		this.email = email;
		this.numeroTelephone = numeroTelephone;
		this.token = token;
		this.etat = etat;
		this.dateCreation = dateCreation;
		this.dateModification = dateModification;
		this.dateMiseAjourPassword = dateMiseAjourPassword;
		this.statutMotdepasse = statutMotdepasse;
		this.passwordUpdateDate = passwordUpdateDate;
		this.newsLetters = newsLetters;
		this.termeEtConditions = termeEtConditions;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenoms() {
		return prenoms;
	}

	public void setPrenoms(String prenoms) {
		this.prenoms = prenoms;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNumeroTelephone() {
		return numeroTelephone;
	}

	public void setNumeroTelephone(String numeroTelephone) {
		this.numeroTelephone = numeroTelephone;
	}

	public String getToken() {
		return token;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}

	public Date getDateMiseAjourPassword() {
		return dateMiseAjourPassword;
	}

	public void setDateMiseAjourPassword(Date dateMiseAjourPassword) {
		this.dateMiseAjourPassword = dateMiseAjourPassword;
	}

	public Integer getStatutMotdepasse() {
		return statutMotdepasse;
	}

	public void setStatutMotdepasse(Integer statutMotdepasse) {
		this.statutMotdepasse = statutMotdepasse;
	}

	public Date getPasswordUpdateDate() {
		return passwordUpdateDate;
	}

	public void setPasswordUpdateDate(Date passwordUpdateDate) {
		this.passwordUpdateDate = passwordUpdateDate;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Utilisateur getCreerPar() {
		return creerPar;
	}

	public void setCreerPar(Utilisateur creerPar) {
		this.creerPar = creerPar;
	}

	public Utilisateur getModifierPar() {
		return modifierPar;
	}

	public void setModifierPar(Utilisateur modifierPar) {
		this.modifierPar = modifierPar;
	}

	public Boolean getNewsLetters() {
		return newsLetters;
	}

	public void setNewsLetters(Boolean newsLetters) {
		this.newsLetters = newsLetters;
	}

	public Boolean getTermeEtConditions() {
		return termeEtConditions;
	}

	public void setTermeEtConditions(Boolean termeEtConditions) {
		this.termeEtConditions = termeEtConditions;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void updateToken() {
		this.token = UUID.randomUUID().toString();
	}

	public void setToken(String token) {
		// this.token = token;
		updateToken();
	}

	public static Utilisateur integrateur(Role role, PasswordEncoder passwordEncoder) {
		Utilisateur user = new Utilisateur();

		user.setUsername("integrateur@dmin.com");
		user.setEmail("integrateur@dmin.com");
		user.setPassword(passwordEncoder.encode("PittikkiMdp2021$"));
		user.setPasswordConfirm(user.getPassword());
		user.setNom("Intégrateur Solution");
		user.setPrenoms("Ebenyx");
		user.setNumeroTelephone("0000000000");
		user.setRole(role);

		return user;
	}

	public static Utilisateur superAdministrateur(Role role, PasswordEncoder passwordEncoder) {
		Utilisateur user = new Utilisateur();

		user.setUsername("adesire@ebenyx.com");
		user.setEmail("adesire@ebenyx.com");
		user.setPassword(passwordEncoder.encode("ebenyxMdp2021$"));
		user.setPasswordConfirm(user.getPassword());
		user.setNom("Super Administrateur");
		user.setPrenoms("Ebenyx");
		user.setNumeroTelephone("0000000000");
		user.setRole(role);

		return user;
	}

	public Utilisateur administrateur() {
		Utilisateur user = new Utilisateur();
		new Role();
		Role admin = Role.administrateur();
		user.setNumeroTelephone("0000000000");
		user.setRole(admin);

		return user;
	}

}
