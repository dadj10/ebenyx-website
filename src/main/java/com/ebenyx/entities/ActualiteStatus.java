package com.ebenyx.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ebenyx.config.Constantes;

@Entity
@Table(name = "actualiteStatus")
public class ActualiteStatus implements Serializable {

	private static final long serialVersionUID = 1L;

	// Attributs
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idActualiteStatus;
	private String nom;
	private String classBootstrap;

	public ActualiteStatus() {
		super();
	}

	public ActualiteStatus(String nom, int etat, Date dateCreation, Date dateModification, String classBootstrap) {
		super();
		this.nom = nom;
		this.classBootstrap = classBootstrap;
	}

	public Long getIdActualiteStatus() {
		return idActualiteStatus;
	}

	public void setIdActualiteStatus(Long idActualiteStatus) {
		this.idActualiteStatus = idActualiteStatus;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getClassBootstrap() {
		return classBootstrap;
	}

	public void setClassBootstrap(String classBootstrap) {
		this.classBootstrap = classBootstrap;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ActualiteStatus published() {
		ActualiteStatus published = new ActualiteStatus();
		published.setNom(Constantes.ACTUALITE_STATUS_PUBLISHED);
		return published;
	}

	public ActualiteStatus brouillon() {
		ActualiteStatus published = new ActualiteStatus();
		published.setNom(Constantes.ACTUALITE_STATUS_BROUILLON);
		return published;
	}

	public ActualiteStatus enAttenteExam() {
		ActualiteStatus published = new ActualiteStatus();
		published.setNom(Constantes.ACTUALITE_STATUS_ATTENTE_EXEMEN);
		return published;
	}

}
