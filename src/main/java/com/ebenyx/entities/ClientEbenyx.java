package com.ebenyx.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "clientEbenyx")
public class ClientEbenyx extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idClientEbenyx;
	private String raisonSociale;
	@Email
	private String email;
	private String telephone =  "00000000";
	private String photo; // (logo pour entreprise)

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateDebutPartenariat = new Date();

	// Constructeur
	public ClientEbenyx() {
		super();
		this.dateDebutPartenariat = new Date();
	}

	public ClientEbenyx(String raisonSociale, Date dateDebutPartenariat, @Email String email, String telephone,
			Date dateCreation, Date dateModification, int etat, boolean enabled, String photo) {
		super();
		this.raisonSociale = raisonSociale;
		this.dateDebutPartenariat = dateDebutPartenariat;
		this.email = email;
		this.telephone = telephone;
		this.photo = photo;
	}

	public Long getIdClientEbenyx() {
		return idClientEbenyx;
	}

	public void setIdClientEbenyx(Long idClientEbenyx) {
		this.idClientEbenyx = idClientEbenyx;
	}

	public String getRaisonSociale() {
		return raisonSociale;
	}

	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}

	public Date getDateDebutPartenariat() {
		return dateDebutPartenariat;
	}

	public void setDateDebutPartenariat(Date dateDebutPartenariat) {
		this.dateDebutPartenariat = dateDebutPartenariat;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

}
