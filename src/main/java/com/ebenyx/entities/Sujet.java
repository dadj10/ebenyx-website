package com.ebenyx.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ebenyx.config.Constantes;

@Entity
@Table(name = "sujet")
public class Sujet extends BaseEntity {

	private static final long serialVersionUID = 1L;

	// Attributs
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idSujet;
	private String nom;

	public Sujet() {
		super();
	}

	public Sujet(String nom) {
		super();
		this.nom = nom;
	}

	public Long getIdSujet() {
		return idSujet;
	}

	public void setIdSujet(Long idSujet) {
		this.idSujet = idSujet;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Sujet blockchain() {
		Sujet blockchain = new Sujet();
		blockchain.setNom(Constantes.SUJET_BLOCKCHAIN);
		return blockchain;
	}

	public Sujet kafka() {
		Sujet kafka = new Sujet();
		kafka.setNom(Constantes.SUJET_KAFKA);
		return kafka;
	}

	public Sujet kubernates() {
		Sujet kub = new Sujet();
		kub.setNom(Constantes.SUJET_KUBERNATES);
		return kub;
	}

}
