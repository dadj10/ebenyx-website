package com.ebenyx.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "abonnesNewsletters")
public class AbonnesNewsletters implements Serializable {

	private static final long serialVersionUID = 1L;

	// Attributs
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idAbonnesNewsletters;
	private String referenceAbonnesNewsletters; // slug de AbonnesNewsletters
	@Email
	private String email;
	private String nom;
	
	@DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private Date dateCreation = new Date();
	@DateTimeFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	private Date dateModification;
	
	private int etat = 1;
	private boolean enabled = Boolean.TRUE;

	// Constructeurs
	public AbonnesNewsletters() {
		super();
		updateReference();
	}

	private void updateReference() {
		this.referenceAbonnesNewsletters = UUID.randomUUID().toString();
	}

	public AbonnesNewsletters(@Email String email, String nom, Boolean enabled, int etat, Date dateCreation, Date dateModification,
			String referenceAbonnesNewsletters) {
		super();
		this.email = email;
		this.nom = nom;
		this.etat = etat;
		this.enabled = enabled;
		this.dateCreation = dateCreation;
		this.dateModification = dateModification;
		this.referenceAbonnesNewsletters = referenceAbonnesNewsletters;
	}

	// Getters & setters
	public Long getIdAbonnesNewsletters() {
		return idAbonnesNewsletters;
	}

	public void setIdAbonnesNewsletters(Long idAbonnesNewsletters) {
		this.idAbonnesNewsletters = idAbonnesNewsletters;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Date getDateModification() {
		return dateModification;
	}

	public void setDateModification(Date dateModification) {
		this.dateModification = dateModification;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getReferenceAbonnesNewsletters() {
		return referenceAbonnesNewsletters;
	}

	public void setReferenceAbonnesNewsletters() {
		// this.referenceAbonnesNewsletters = referenceAbonnesNewsletters;
		updateReference();
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
