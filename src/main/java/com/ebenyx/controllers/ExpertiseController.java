package com.ebenyx.controllers;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ebenyx.repositories.ParametreRepository;

@Controller
@RequestMapping(value = "/expertise")
public class ExpertiseController {
	
	// Logger
	private static final Logger log = LoggerFactory.getLogger(ExpertiseController.class);
	
	@Autowired
	private ParametreRepository parametreRepos;
	
	@GetMapping()
	public String page(Model model, HttpSession session) {
		String page = "redirect:/";
		try {
			// titre et breadcrumb de la page
			model.addAttribute("page_title", "Expertise");
			model.addAttribute("active_menu", "expertise");
			
			session.setAttribute("Parametre", parametreRepos.get());
			
			page = "expertise/index";
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return page;
	}



}
