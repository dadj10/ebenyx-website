package com.ebenyx.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ebenyx.config.Msg;
import com.ebenyx.entities.ContactMessage;
import com.ebenyx.entities.Sujet;
import com.ebenyx.repositories.ContactMessageRepository;
import com.ebenyx.repositories.ParametreRepository;
import com.ebenyx.repositories.SujetRepository;

@Controller
@RequestMapping(value = "/contact")
public class ContactController {

	// Logger
	private static final Logger log = LoggerFactory.getLogger(ContactController.class);

	// Inversion de controle
	@Autowired
	private ParametreRepository parametreRepos;
	@Autowired
	private SujetRepository sujetRepos;
	@Autowired
	private ContactMessageRepository contactMessageRepos;

	@GetMapping()
	public String page(Model model, HttpSession session) {
		String page = "redirect:/";
		try {
			// titre et breadcrumb de la page
			model.addAttribute("page_title", "Contactez nous");
			model.addAttribute("active_menu", "contact");

			session.setAttribute("Parametre", parametreRepos.get());

			// je recupere la liste des sujet
			List<Sujet> sujets = null;
			sujets = sujetRepos.findAllItem();
			model.addAttribute("Sujets", sujets);

			// Je cree une nouvelle instance de ContactMessage();
			ContactMessage message = new ContactMessage();
			model.addAttribute("ContactMessage", message);

			page = "contact/index";

		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		return page;
	}

	/**
	 * Fonction de création d'un offre d'emploi (Entreprise)
	 */
	@PostMapping(value = "/create")
	public String create(@Valid ContactMessage a, HttpSession session, RedirectAttributes redirAttrs, Model model,
			BindingResult bindingResult) {
		String page = "redirect:/contact";
		ContactMessage r = null;
		try {
			if (bindingResult.hasErrors()) {
				redirAttrs.addFlashAttribute("error", Msg.DMD_SEND_ERROR);
				return page;
			}

			r = contactMessageRepos.save(a);

			if (r != null) {
				redirAttrs.addFlashAttribute("success", Msg.DMD_SEND_SUCCESS);
			} else
				redirAttrs.addFlashAttribute("error", Msg.DMD_SEND_ERROR);

		} catch (Exception e) {
			redirAttrs.addFlashAttribute("error", Msg.DMD_SEND_ERROR);
			e.printStackTrace();
			log.error(e.getMessage());
		}

		return page;
	}

}
