package com.ebenyx.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ebenyx.config.Constantes;
import com.ebenyx.entities.Actualite;
import com.ebenyx.entities.ActualiteStatus;
import com.ebenyx.repositories.ActualiteRepository;
import com.ebenyx.repositories.ActualiteStatusRepository;
import com.ebenyx.repositories.CertificationRepository;
import com.ebenyx.repositories.ClientEbenyxRepository;
import com.ebenyx.repositories.ParametreRepository;

@Controller
@RequestMapping(value = "/")
public class AccueilController {

	// Logger
	private static final Logger log = LoggerFactory.getLogger(AccueilController.class);

	@Autowired
	private ParametreRepository parametreRepos;
	@Autowired
	private ActualiteRepository actualiteRepos;
	@Autowired
	private ActualiteStatusRepository actualiteStatusRepos;
	@Autowired
	private ClientEbenyxRepository clientEbenyxRepos;
	@Autowired
	private CertificationRepository certificationRepos;
	
	@Value("${dir.blog}")
	private String blogDirectory;
	
	@Value("${dir.client}")
	private String clientDirectory;
	
	@Value("${dir.certification}")
	private String certificationDirectory;

	@GetMapping()
	public String page(Model model, HttpSession session) {
		String page = "accueil/index";
		try {
			// titre et breadcrumb de la page
			model.addAttribute("page_title", "Accueil");
			model.addAttribute("active_menu", "home");

			session.setAttribute("Parametre", parametreRepos.get());
			
			// Je recupere la liste de 3 derniere actualites
			ActualiteStatus published = actualiteStatusRepos.findByNom(Constantes.ACTUALITE_STATUS_PUBLISHED.toLowerCase());
			List<Actualite> lasts3Actualites = null;
			lasts3Actualites = actualiteRepos.findLasts3ActuOfBlog(published.getIdActualiteStatus());
			model.addAttribute("Lasts3Actualites", lasts3Actualites);
			
			model.addAttribute("ClientEbenyxs", clientEbenyxRepos.findAllItems());
			
			// Liste de certifications
			model.addAttribute("Certifications", certificationRepos.findAllItems());
			
			return page;

		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		
		return page;
	}
	
	/**
	 * Cette fonction permet de recuperer le logo
	 */
	@GetMapping(value = "/getlogoclient", produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public byte[] getPhotoLogoClient(String username) throws FileNotFoundException, IOException {
		File file = new File(clientDirectory + username); // Je recupère l'image
		if (!file.exists()) {
			// photo par defaut
			String photo_defaut = parametreRepos.findPhotoDefaut();
			file = new File(clientDirectory + photo_defaut);
		}
		return IOUtils.toByteArray(new FileInputStream(file));
	}
	
	/**
	 * Cette fonction permet de recuperer la photo
	 */
	@GetMapping(value = "/getlogocertifiction", produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public byte[] getPhotoCerficiation(String filename) throws FileNotFoundException, IOException {
		File file = new File(certificationDirectory + filename);
		if (!file.exists()) {
			// photo par defaut
			String photo_defaut = parametreRepos.findCertificatDefaut();
			file = new File(certificationDirectory + photo_defaut);
		}
		return IOUtils.toByteArray(new FileInputStream(file));
	}
}
