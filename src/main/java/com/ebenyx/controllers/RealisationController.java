package com.ebenyx.controllers;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ebenyx.repositories.ParametreRepository;

@Controller
@RequestMapping(value = "/realisations")
public class RealisationController {
	
	// Logger
	private static final Logger log = LoggerFactory.getLogger(RealisationController.class);
	
	@Autowired
	private ParametreRepository parametreRepos;
	
	@GetMapping()
	public String page(Model model, HttpSession session) {
		String page = "redirect:/";
		try {
			// titre et breadcrumb de la page
			model.addAttribute("page_title", "Nos réalisations");
			model.addAttribute("active_menu", "realisations");
			
			session.setAttribute("Parametre", parametreRepos.get());
			
			page = "realisations/index";
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return page;
	}
	
	@GetMapping(value = "/oracle")
	public String pageOracle(Model model, HttpSession session) {
		String page = "redirect:/";
		try {
			// titre et breadcrumb de la page
			model.addAttribute("page_title", "Nos réalisations");
			model.addAttribute("active_menu", "realisations");
			
			session.setAttribute("Parametre", parametreRepos.get());
			
			page = "realisations/oracle";
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return page;
	}
	
	@GetMapping(value = "/transformation-digitale")
	public String pageTansfornationDigital(Model model, HttpSession session) {
		String page = "redirect:/";
		try {
			// titre et breadcrumb de la page
			model.addAttribute("page_title", "Nos réalisations");
			model.addAttribute("active_menu", "realisations");
			
			session.setAttribute("Parametre", parametreRepos.get());
			
			page = "realisations/transformationdigitale";
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return page;
	}
	
	@GetMapping(value = "/cybersecutite")
	public String pageCybersecutite(Model model, HttpSession session) {
		String page = "redirect:/";
		try {
			// titre et breadcrumb de la page
			model.addAttribute("page_title", "Nos réalisations");
			model.addAttribute("active_menu", "realisations");
			
			session.setAttribute("Parametre", parametreRepos.get());
			
			page = "realisations/cybersecutite";
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return page;
	}



}
