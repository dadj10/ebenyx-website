package com.ebenyx.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ebenyx.config.GetCurrentUser;
import com.ebenyx.entities.Utilisateur;
import com.ebenyx.repositories.ParametreRepository;

@Controller
@RequestMapping(value = "/faqs")
public class FaqsController {

	// Logger
	private static final Logger log = LoggerFactory.getLogger(FaqsController.class);

	// Inversion de controle
	@Autowired
	private ParametreRepository parametreRepos;

	// Je cré une nouvelle instance de mon objet qui recupère l'utilisateur connecté
	GetCurrentUser user = new GetCurrentUser();
	Utilisateur userConnected = null;

	Date dateCourante = new Date();
	SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");

	@GetMapping()
	public String page(Model model, HttpSession session) {
		String page = "redirect:/";
		try {
			// titre et breadcrumb de la page
			model.addAttribute("page_title", "Questions fréquemment posées");
			model.addAttribute("active_menu", "faqs");

			session.setAttribute("Parametre", parametreRepos.get());

			page = "faqs/index";
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return page;
	}

}
