package com.ebenyx.controllers.admin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ebenyx.config.Constantes;
import com.ebenyx.config.GetCurrentUser;
import com.ebenyx.config.Msg;
import com.ebenyx.config.Utils;
import com.ebenyx.entities.Actualite;
import com.ebenyx.entities.ActualiteStatus;
import com.ebenyx.entities.Utilisateur;
import com.ebenyx.repositories.ActualiteRepository;
import com.ebenyx.repositories.ActualiteStatusRepository;
import com.ebenyx.repositories.ClientEbenyxRepository;
import com.ebenyx.repositories.ParametreRepository;
import com.ebenyx.repositories.UtilisateurRepository;

@Controller
@RequestMapping(value = "/actualite")
public class ActualiteController {

	// Logger
	private static final Logger log = LoggerFactory.getLogger(ActualiteController.class);

	@Autowired
	private ParametreRepository parametreRepos;
	@Autowired
	private ClientEbenyxRepository clientEbenyxRepos;
	@Autowired
	private UtilisateurRepository utilisateurRepos;
	@Autowired
	private ActualiteRepository actualiteRepos;
	@Autowired
	private ActualiteStatusRepository actualiteStatusRepos;
	
	@Value("${dir.blog}")
	private String blogDirectory;

	// Je cré une nouvelle instance de mon objet qui recupère l'utilisateur connecté
	GetCurrentUser user = new GetCurrentUser();
	Utilisateur userConnected = null;

	@GetMapping()
	public String page(Model model, HttpSession session) {
		String page = "redirect:/login";
		try {
			if (user.getUserConnected() == null) {
				return page;
			}

			// Je recupère les informations de l'utilisateur connecté.
			userConnected = utilisateurRepos.findUtilisateurByUsername(user.getUserConnected());
			session.setAttribute("Utilisateur", userConnected);

			// Si l'utilisateur connecté a le ROLE_SUPER_ADMINISTRATEUR ou ROLE_ADMINISTRATEUR
			if (userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_SUPER_ADMINISTRATEUR)
					|| userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_ADMINISTRATEUR)) {

				// J'active le menu
				model.addAttribute("active_menu", "actualite");
				model.addAttribute("page_title", "Actualités Ebenyx");

				// Je recupère la liste des articles
				List<Actualite> actualites = null;
				actualites = actualiteRepos.findAllActualiteOrderByIdDesc();
				model.addAttribute("Actualites", actualites);

				page = "admin/actualite/index";
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		return page;

	}

	/**
	 * Retourne le formulaire de création d'une actualité
	 */
	@GetMapping(value = "/formcreate")
	public String formcreate(Model model, HttpSession session) {
		String page = "redirect:/actualite";
		try {
			if (user.getUserConnected() == null) {
				return page;
			}
			
			// Je recupère les informations de l'utilisateur connecté.
			userConnected = utilisateurRepos.findUtilisateurByUsername(user.getUserConnected());
			session.setAttribute("Utilisateur", userConnected);
			
			// Si l'utilisateur connecté a le ROLE_SUPER_ADMINISTRATEUR ou ROLE_ADMINISTRATEUR
			if (userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_SUPER_ADMINISTRATEUR)
					|| userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_ADMINISTRATEUR)) {
				
				// J'active le menu
				model.addAttribute("active_menu", "actualite");
				model.addAttribute("page_title", "Actualités Ebenyx");

				model.addAttribute("author", userConnected.getUsername());

				// Je recupère la liste des status articles
				List<ActualiteStatus> actualiteStatus = actualiteStatusRepos.findActualiteStatusOrderByNomAsc();
				model.addAttribute("ActualiteStatus", actualiteStatus);

				Actualite brouillon = new Actualite().brouillon();
				model.addAttribute("Actualite", brouillon);

				page = "admin/actualite/create";
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		return page;
	}
	
	/**
	 * Fonction create item
	 */
	@PostMapping(value = "/create")
	public String create(@Valid Actualite a,
			@RequestParam(name = "file1") MultipartFile photoActualite,
			Model model, HttpSession session, RedirectAttributes redirAttrs, BindingResult bindingResult) {
		Actualite retour = null;
		String page = "redirect:/actualite";
		try {
			if (bindingResult.hasErrors()) {
				redirAttrs.addFlashAttribute("error", Msg.CREATE_ERROR);
				return page;
			}
			
			if (user.getUserConnected() == null) {
				return page;
			}	

			// Je recupère les informations de l'utilisateur connecté.
			userConnected = utilisateurRepos.findUtilisateurByUsername(user.getUserConnected());
			session.setAttribute("Utilisateur", userConnected);

			// Si l'utilisateur connecté a le ROLE_SUPER_ADMINISTRATEUR ou ROLE_ADMINISTRATEUR
			if (userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_SUPER_ADMINISTRATEUR)
					|| userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_ADMINISTRATEUR)) {
				
				// Image de mise en avant de l'actualit
				if (!(photoActualite.isEmpty())) {
					a.setPhotoActualite(photoActualite.getOriginalFilename());
					photoActualite.transferTo(new File(blogDirectory + photoActualite.getOriginalFilename()));
				}

				// Gestion du resumé de l'article
				Long longeurResumerArticle = parametreRepos.getLongeurResumerArticle();
				if (longeurResumerArticle == null)
					longeurResumerArticle = Constantes.LONGUEUR_RESUME_ARTICLE;

				String resume = null;
				
				String content_str = a.contenu_to_String();
				if (content_str.length() >= longeurResumerArticle)
					resume = Utils.getResume(content_str, longeurResumerArticle.intValue());
				else
					resume = content_str;

				a.setResumeActualite(resume);
				a.getContenuActualite();

				a.setSoeUrl();
				a.setCreerPar(userConnected);
				a.setDateCreation(new Date());
				a.setEtat(1);

				// Gestion de la date de parution
				a.setDateDeParution(a.getDateDeParution());

				retour = actualiteRepos.save(a);

				if (retour != null)
					redirAttrs.addFlashAttribute("success", Msg.CREATE_SUCCESS);
				else
					redirAttrs.addFlashAttribute("error", Msg.CREATE_ERROR);
			}
		
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		return page;
	}
	
	/**
	 * Fonction find one item
	 */
	public Actualite findOne(Long idActualite) {
		Actualite actualite = null;
		try {
			actualite = actualiteRepos.findById(idActualite).orElse(null);
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return actualite;
	}
	
	/**
	 * Fonction form update
	 */
	@GetMapping(value = "/formupdate")
	public String formupdate(Long id, RedirectAttributes redirAttrs, Model model, HttpSession session) {
		String page = "redirect:/actualite";
		try {
			if (user.getUserConnected() == null) {
				return page;
			}
			
			// Je recupère les informations de l'utilisateur connecté.
			userConnected = utilisateurRepos.findUtilisateurByUsername(user.getUserConnected());
			session.setAttribute("Utilisateur", userConnected);

			// Si l'utilisateur connecté a le ROLE_SUPER_ADMINISTRATEUR ou ROLE_ADMINISTRATEUR
			if (userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_SUPER_ADMINISTRATEUR)
					|| userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_ADMINISTRATEUR)) {
				
				// J'active le menu
				model.addAttribute("active_menu", "actualite");
				model.addAttribute("page_title", "Actualités Ebenyx");
				
				model.addAttribute("author", userConnected.getUsername());

				List<ActualiteStatus> actualiteStatus = actualiteStatusRepos.findActualiteStatusOrderByNomAsc();
				model.addAttribute("ActualiteStatus", actualiteStatus);

				model.addAttribute("Actualite", findOne(id));

				page = "admin/actualite/update";

			}
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		return page;
	}
	
	/**
	 * Fonction update item
	 */
	@PostMapping(value = "/update")
	public String update(@Valid Actualite a,
			@RequestParam(name = "file1") MultipartFile photoActualite,
			
			BindingResult bindingResult, HttpSession session, RedirectAttributes redirAttrs, Model model) {
		Actualite retour = null;
		String page = "redirect:/actualite";
		try {
			if (user.getUserConnected() == null) {
				return page;
			}
			
			if (bindingResult.hasErrors()) {
				redirAttrs.addFlashAttribute("error", "Erreur lors de l'enregistrement, veuillez réessayer à nouveau.");
				return page;
			}

			// Je recupère les informations de l'utilisateur connecté.
			userConnected = utilisateurRepos.findUtilisateurByUsername(user.getUserConnected());

			// Si l'utilisateur connecté a le ROLE_SUPER_ADMINISTRATEUR ou ROLE_ADMINISTRATEUR
			if (userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_SUPER_ADMINISTRATEUR)
					|| userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_ADMINISTRATEUR)) {

				Actualite actualite = null;
				actualite = findOne(a.getIdActualite());

				if (actualite != null) {
					a.setReferenceActualite(actualite.getReferenceActualite());
					a.setCreerPar(actualite.getCreerPar());
					a.setModifierPar(userConnected);
					a.setDateModification(new Date());
					a.setSoeUrl();
					
					// Image de mise en avant de l'actualité
					if (!(photoActualite.isEmpty())) {
						a.setPhotoActualite(photoActualite.getOriginalFilename());
						photoActualite.transferTo(new File(blogDirectory + photoActualite.getOriginalFilename()));
					} else {
						a.setPhotoActualite(actualite.getPhotoActualite());
					}

					// Gestion du resumé de l'article
					Long longeurResumerArticle = parametreRepos.getLongeurResumerArticle();
					if (longeurResumerArticle == null)
						longeurResumerArticle = Constantes.LONGUEUR_RESUME_ARTICLE;
					
					String resume = null;
					String content_str = a.contenu_to_String();
					if (content_str.length() >= longeurResumerArticle)
						resume = Utils.getResume(content_str, longeurResumerArticle.intValue());
					else
						resume = content_str;
					
					a.setResumeActualite(resume);

					retour = actualiteRepos.save(a);
				}

				if (retour != null)
					redirAttrs.addFlashAttribute("success", Msg.UPDATE_SUCCESS);
				else
					redirAttrs.addFlashAttribute("error", Msg.UPDATE_ERROR);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		return page;
	}
	
	/**
	 * Fonction de suppression d'une actualité
	 */
	@GetMapping(value = "/delete")
	public String delete(Long id, RedirectAttributes redirAttrs) {
		String page = "redirect:/actualite";
		Actualite a = null;
		try {
			if (user.getUserConnected() == null) {
				return page;
			}
			
			// Je recupère les informations de l'utilisateur connecté.
			userConnected = utilisateurRepos.findUtilisateurByUsername(user.getUserConnected());
			
			// Si l'utilisateur connecté a le ROLE_SUPER_ADMINISTRATEUR ou ROLE_ADMINISTRATEUR
			if (userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_SUPER_ADMINISTRATEUR)
					|| userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_ADMINISTRATEUR)) {
				
				a = findOne(id);
				if (a != null) {
					a.setEtat(-1);
					a.setDateSuppression(new Date());
					a.setSupprimerPar(userConnected);
					a = actualiteRepos.save(a);

					if (a == null) {
						redirAttrs.addFlashAttribute("error", Msg.REMOVE_ERROR);
					} else
						redirAttrs.addFlashAttribute("success", Msg.REMOVE_SUCCESS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return page;
	}
	
	/**
	 * Fonctin get picture
	 */
	@GetMapping(value = "/getphoto", produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public byte[] getPhoto(String originalFilename) throws FileNotFoundException, IOException {
		File file = new File(blogDirectory + originalFilename); // Je recupère l'image
		if (!file.exists()) {
			// photo par defaut
			String blog_image_defaut = parametreRepos.findBlogImageDefaut();
			file = new File(blogDirectory + blog_image_defaut);
		}
		return IOUtils.toByteArray(new FileInputStream(file));
	}
}
