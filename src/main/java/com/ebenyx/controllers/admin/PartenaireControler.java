package com.ebenyx.controllers.admin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ebenyx.config.Constantes;
import com.ebenyx.config.GetCurrentUser;
import com.ebenyx.config.Msg;
import com.ebenyx.entities.Partenaire;
import com.ebenyx.entities.Utilisateur;
import com.ebenyx.repositories.ParametreRepository;
import com.ebenyx.repositories.PartenaireRepository;
import com.ebenyx.repositories.UtilisateurRepository;

@Controller
@RequestMapping(value = "/partenaire")
public class PartenaireControler {

	// Logger
	private static final Logger log = LoggerFactory.getLogger(PartenaireControler.class);

	@Autowired
	private ParametreRepository parametreRepos;
	@Autowired
	private PartenaireRepository partenaireRepos;
	@Autowired
	private UtilisateurRepository utilisateurRepos;

	// Je cré une nouvelle instance de mon objet qui recupère l'utilisateur connecté
	GetCurrentUser user = new GetCurrentUser();
	Utilisateur userConnected = null;

	@Value("${dir.partenaire}")
	private String partenaireDirectory;

	@GetMapping()
	public String page(Model model, HttpSession session) {
		String page = "redirect:/login";
		try {
			if (user.getUserConnected() == null) {
				return page;
			}

			// Je recupère les informations de l'utilisateur connecté.
			userConnected = utilisateurRepos.findUtilisateurByUsername(user.getUserConnected());
			session.setAttribute("Utilisateur", userConnected);

			// Si l'utilisateur connecté a le ROLE_SUPER_ADMINISTRATEUR ou
			// ROLE_ADMINISTRATEUR
			if (userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_SUPER_ADMINISTRATEUR)
					|| userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_ADMINISTRATEUR)) {

				// J'active le menu
				model.addAttribute("active_menu", "partenaire");
				model.addAttribute("page_title", "Partenaires Ebenyx");

				model.addAttribute("Partenaires", partenaireRepos.findAllItems());

				page = "admin/partenaires/index";
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		return page;
	}

	/**
	 * Fonction form create new iten
	 */
	@GetMapping(value = "/formcreate")
	public String formcreate(Model model, HttpSession session) {
		String page = "redirect:/partenaire";
		try {
			// Je recherche toutes les informations de l'utilisateur connecté
			userConnected = utilisateurRepos.findUtilisateurByUsername(user.getUserConnected());

			// Si l'utilisateur connecté a le ROLE_SUPER_ADMINISTRATEUR ou
			// ROLE_ADMINISTRATEUR
			if (userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_SUPER_ADMINISTRATEUR)
					|| userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_ADMINISTRATEUR)) {

				// J'envoi cet utilisateur dans ma vue
				session.setAttribute("Utilisateur", userConnected);

				// J'active le menu
				model.addAttribute("active_menu", "partenaire");
				model.addAttribute("page_title", "Partenaires Ebenyx");

				model.addAttribute("Partenaire", new Partenaire());

				page = "admin/partenaires/create";
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		return page;
	}

	/**
	 * Fonction create item
	 */
	@PostMapping(value = "/create")
	public String create(@Valid Partenaire a, @RequestParam(name = "picture") MultipartFile picture,
			BindingResult bindingResult, HttpSession session, RedirectAttributes redirAttrs, Model model) {
		Partenaire r = null;
		String page = "redirect:/partenaire";
		try {
			if (bindingResult.hasErrors()) {
				redirAttrs.addFlashAttribute("error", Msg.CREATE_ERROR);
				return page;
			}

			r = findOne(a.getNom().toLowerCase());
			if (r != null) {
				redirAttrs.addFlashAttribute("error", Msg.ITEM_EXISTE);
				return page;
			}

			// logo
			if (!(picture.isEmpty())) {
				a.setPhoto(picture.getOriginalFilename());
				picture.transferTo(new File(partenaireDirectory + picture.getOriginalFilename()));
			}
			a.setSoeUrl();

			r = partenaireRepos.save(a);

			if (r == null)
				redirAttrs.addFlashAttribute("error", Msg.CREATE_ERROR);
			else
				redirAttrs.addFlashAttribute("success", Msg.CREATE_SUCCESS);

		} catch (Exception e) {
			redirAttrs.addFlashAttribute("error", Msg.CREATE_ERROR);
			e.printStackTrace();
			log.error(e.getMessage());
			return page;
		}

		return page;
	}

	/**
	 * Fonction find one item
	 */
	public Partenaire findOne(String name) {
		Partenaire a = null;
		try {
			a = partenaireRepos.findItemByName(name);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return a;
	}

	/**
	 * Fonction form update item
	 */
	@GetMapping(value = "/formupdate")
	public String formupdate(Long id, RedirectAttributes redirAttrs, Model model, HttpSession session) {
		String page = "redirect:/partenaire";
		try {
			// Je recupère les informations de l'utilisateur connecté.
			userConnected = utilisateurRepos.findUtilisateurByUsername(user.getUserConnected());

			if (userConnected == null) {
				return page;
			}

			// Si l'utilisateur connecté a le ROLE_PARTICULIER
			if (userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_SUPER_ADMINISTRATEUR)
					|| userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_ADMINISTRATEUR)) {
				// J'envoi cet utilisateur dans ma vue
				session.setAttribute("Utilisateur", userConnected);

				Partenaire t = null;
				t = partenaireRepos.findItemById(id);

				if (t == null) {
					return page;
				}

				// J'active le menu
				model.addAttribute("active_menu", "partenaire");
				model.addAttribute("page_title", "Partenaires Ebenyx");

				model.addAttribute("Partenaire", t);
				page = "admin/partenaires/update";

			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}

		return page;
	}

	/**
	 * Fonction update item
	 */
	@PostMapping(value = "/update")
	public String update(@Valid Partenaire a, @RequestParam(name = "picture") MultipartFile picture,
			BindingResult bindingResult, HttpSession session, RedirectAttributes redirAttrs, Model model) {
		String page = "redirect:/partenaire";
		Partenaire r = null;
		try {
			if (bindingResult.hasErrors()) {
				redirAttrs.addFlashAttribute("error", Msg.CREATE_ERROR);
				return page;
			}

			if (user.getUserConnected() == null) {
				return page;
			}

			// Je recupère les informations de l'utilisateur connecté.
			userConnected = utilisateurRepos.findUtilisateurByUsername(user.getUserConnected());
			session.setAttribute("Utilisateur", userConnected);

			// Si l'utilisateur connecté a le ROLE_SUPER_ADMINISTRATEUR ou ROLE_ADMINISTRATEUR
			if (userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_SUPER_ADMINISTRATEUR)
					|| userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_ADMINISTRATEUR)) {

				r = partenaireRepos.findItemById(a.getIdPartenaire());
				if (r != null) {

					a.setCreerPar(a.getCreerPar());
					a.setModifierPar(userConnected);
					a.setDateModification(new Date());

					// logo
					if (!(picture.isEmpty())) {
						a.setPhoto(picture.getOriginalFilename());
						picture.transferTo(new File(partenaireDirectory + picture.getOriginalFilename()));
					} else {
						a.setPhoto(r.getPhoto());
					}
					a.setSoeUrl();

					r = partenaireRepos.save(a);
				}

				if (r != null) {
					redirAttrs.addFlashAttribute("success", Msg.UPDATE_SUCCESS);
				} else {
					redirAttrs.addFlashAttribute("error", Msg.UPDATE_ERROR);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		return page;
	}

	/**
	 * Fonction active/desactive item
	 */
	@GetMapping(value = "/active")
	public String active(Long id, Model model, HttpSession session, RedirectAttributes redirAttrs) {
		String page = "redirect:/client";
		Partenaire retour = null;
		try {
			// Je recherche toutes les informations de l'utilisateur connecté
			userConnected = utilisateurRepos.findUtilisateurByUsername(user.getUserConnected());

			// Si l'utilisateur connecté a le ROLE_SUPER_ADMINISTRATEUR ou
			// ROLE_ADMINISTRATEUR
			if (userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_SUPER_ADMINISTRATEUR)
					|| userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_ADMINISTRATEUR)) {

				// J'envoi cet utilisateur dans ma vue
				session.setAttribute("Utilisateur", userConnected);
				Partenaire t = null;

				t = partenaireRepos.findItemById(id);
				if (t != null) {
					if (t.getEtat() == 0) {
						t.setEtat(1);
					} else {
						t.setEtat(0);
					}
					t.setDateModification(new Date());
					retour = partenaireRepos.save(t);
				}

				if (retour != null) {
					if (retour.getEtat() == 1) {
						redirAttrs.addFlashAttribute("success", Msg.SUCCESS);
					} else {
						redirAttrs.addFlashAttribute("success", Msg.SUCCESS);
					}
				} else {
					redirAttrs.addFlashAttribute("error", Msg.ERROR);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}

		return page;
	}

	/**
	 * Cette fonction permet de recuperer la photo
	 */
	@GetMapping(value = "/getlogopartenaire", produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public byte[] getPhoto(String filename) throws FileNotFoundException, IOException {
		File file = new File(partenaireDirectory + filename); // Je recupère l'image
		if (!file.exists()) {
			// photo par defaut
			String photo_defaut = parametreRepos.findPartenaireDefaut();
			file = new File(partenaireDirectory + photo_defaut);
		}
		return IOUtils.toByteArray(new FileInputStream(file));
	}

}
