package com.ebenyx.controllers.admin;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ebenyx.config.GetCurrentUser;
import com.ebenyx.entities.Utilisateur;
import com.ebenyx.repositories.ParametreRepository;
import com.ebenyx.repositories.UtilisateurRepository;

@Controller
@RequestMapping(value = "/tableau_de_bord")
public class DarshbordController {

	// Logger
	private static final Logger log = LoggerFactory.getLogger(DarshbordController.class);

	@Autowired
	private ParametreRepository parametreRepos;
	@Autowired
	private UtilisateurRepository utilisateurRepos;

	// Je cré une nouvelle instance de mon objet qui recupère l'utilisateur connecté
	GetCurrentUser user = new GetCurrentUser();
	Utilisateur userConnected = null;

	@GetMapping()
	public String page(Model model, HttpSession session) {
		String page = "redirect:/";
		try {
			// Je recupère les informations de l'utilisateur connecté.
			userConnected = utilisateurRepos.findUtilisateurByUsername(user.getUserConnected());

			if (userConnected == null) {
				return "login";
			}
			
			// J'active le menu
			model.addAttribute("active_menu", "tableau_de_bord");

			// J'envoi cet utilisateur dans ma vue
			session.setAttribute("Utilisateur", userConnected);
			page = "admin/dashboard/index";

		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		return page;
	}

}
