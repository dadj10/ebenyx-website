package com.ebenyx.controllers.admin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ebenyx.config.Constantes;
import com.ebenyx.config.GetCurrentUser;
import com.ebenyx.config.Msg;
import com.ebenyx.entities.Parametre;
import com.ebenyx.entities.Utilisateur;
import com.ebenyx.repositories.ParametreRepository;
import com.ebenyx.repositories.UtilisateurRepository;
import com.fasterxml.jackson.core.JsonProcessingException;

@Controller
@RequestMapping(value = "/parametre")
public class ParametreController {

	// Logger
	private static final Logger log = LoggerFactory.getLogger(ParametreController.class);

	// Invertion de control.
	@Autowired
	private UtilisateurRepository utilisateurRepos;
	@Autowired
	private ParametreRepository parametreRepos;

	@Value("${dir.photos}")
	private String photoDirectory;

	@Value("${dir.blog}")
	private String blogDirectory;
	
	@Value("${dir.client}")
	private String clientDirectory;
	
	@Value("${dir.solution}")
	private String solutionDirectory;
	
	@Value("${dir.realisation}")
	private String realisationDirectory;
	
	@Value("${dir.brochure}")
	private String brochureDirectory;

	// Je cré une nouvelle instance de mon objet qui recupère l'utilisateur connecté
	GetCurrentUser user = new GetCurrentUser();
	Utilisateur userConnected = null;

	Date dateCourante = new Date();
	SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * Cette fonction affiche les informations paramètres.
	 */
	@GetMapping()
	public String page(Model model, HttpSession session) throws JsonProcessingException {
		String page = "redirect:/";
		try {
			// Je recupère les informations de l'utilisateur connecté.
			userConnected = utilisateurRepos.findUtilisateurByUsername(user.getUserConnected());
			if (userConnected != null) {

				// Si l'utilisateur connecté a le ROLE_PARTICULIER
				if (userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_INTEGRATEUR)
						|| userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_SUPER_ADMINISTRATEUR)) {
					// J'envoi cet utilisateur dans ma vue
					session.setAttribute("Utilisateur", userConnected);

					// J'active le menu
					model.addAttribute("active_menu", "parametre");
					model.addAttribute("page_title", "Paramètrage système");

					model.addAttribute("Parametre", parametreRepos.get());

					page = "admin/parametre/index";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}

		return page;
	}

	/**
	 * Cette fonction permet de retrouver un Parametre par son identifiant.
	 */
	public Parametre findOne(Long id) {
		Parametre parametre = null;
		try {
			parametre = parametreRepos.findById(id).orElse(parametre);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return parametre;
	}

	/**
	 * Fonction permettant de mettre à jours les parametres
	 */
	@PostMapping(value = "/update")
	public String update(@Valid Parametre a,
			@RequestParam(name = "blogImageDefautFile") MultipartFile blogImageDefaut,
			@RequestParam(name = "clientImageDefautFile") MultipartFile clientImageDefaut,
			@RequestParam(name = "solutionImageDefautFile") MultipartFile solutionImageDefaut,
			@RequestParam(name = "realisationImageDefautFile") MultipartFile realisationImageDefaut,
			@RequestParam(name = "brochureFile") MultipartFile brochure,
			Model model, HttpSession session, RedirectAttributes redirAttrs, BindingResult bindingResult) {
		String page = "redirect:/parametre";
		Parametre p = null;
		try {
			if (bindingResult.hasErrors()) {
				redirAttrs.addFlashAttribute("error", Msg.UPDATE_ERROR);
				return page;
			}

			// Je recherche toutes les informations de l'utilisateur connecté
			userConnected = utilisateurRepos.findUtilisateurByUsername(user.getUserConnected());
			// J'envoi cet utilisateur dans ma vue
			session.setAttribute("Utilisateur", userConnected);
			if (userConnected != null) {

				// Si l'utilisateur connecté a le ROLE_PARTICULIER
				if (userConnected.getRole().getRole().equalsIgnoreCase(Constantes.ROLE_SUPER_ADMINISTRATEUR)) {
					p = findOne(a.getId());
					if (p != null) {
						a.setFondateur(p.getFondateur());
						a.setMotFondateur(p.getMotFondateur());
						a.setPhotoDefaut(p.getPhotoDefaut());
						
						// blogImageDefaut
						if (!(blogImageDefaut.isEmpty())) {
							a.setBlogImageDefaut(blogImageDefaut.getOriginalFilename());
							blogImageDefaut.transferTo(new File(blogDirectory + blogImageDefaut.getOriginalFilename()));
						}
						
						// clientImageDefaut
						if (!(clientImageDefaut.isEmpty())) {
							a.setClientImageDefaut(clientImageDefaut.getOriginalFilename());
							clientImageDefaut.transferTo(new File(clientDirectory + clientImageDefaut.getOriginalFilename()));
						}
						
						// solutionImageDefaut
						if (!(solutionImageDefaut.isEmpty())) {
							a.setSolutionImageDefaut(solutionImageDefaut.getOriginalFilename());
							solutionImageDefaut.transferTo(new File(solutionDirectory + solutionImageDefaut.getOriginalFilename()));
						}
						
						// solutionImageDefaut
						if (!(realisationImageDefaut.isEmpty())) {
							a.setRealisationImageDefaut(realisationImageDefaut.getOriginalFilename());
							realisationImageDefaut.transferTo(new File(realisationDirectory + realisationImageDefaut.getOriginalFilename()));
						}
						
						// brochure
						if (!(brochure.isEmpty())) {
							a.setBrochure(brochure.getOriginalFilename());
							brochure.transferTo(new File(brochureDirectory + brochure.getOriginalFilename()));
						}
						
						p = parametreRepos.save(a);
					}
				}
			}

			if (p != null) {
				redirAttrs.addFlashAttribute("success", Msg.UPDATE_SUCCESS);
			} else {
				redirAttrs.addFlashAttribute("error", Msg.UPDATE_ERROR);
			}
		
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}

		return page;
	}

	@GetMapping(value = "/getphotodefaut", produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public byte[] getPhotoDefaut() throws FileNotFoundException, IOException {
		String photo_defaut = parametreRepos.findPhotoDefaut();
		File file = new File(photoDirectory + photo_defaut); // Je recupère l'image
		return IOUtils.toByteArray(new FileInputStream(file));
	}
	
	@GetMapping(value = "/getblogimagedefaut", produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public byte[] getBlogImageDefaut() throws FileNotFoundException, IOException {
		String blog_image_defaut = parametreRepos.findBlogImageDefaut();
		File file = new File(blogDirectory + blog_image_defaut); // Je recupère l'image
		return IOUtils.toByteArray(new FileInputStream(file));
	}

}
